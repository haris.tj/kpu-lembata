@include('layout.header')
<!-- container -->
<style type="text/css">
	.pagination li{
		list-style-type: none;
		margin:5px;
	}
</style>
<div class="container">
	<!-- row -->
	<div class="row">
		<div class="col-md-12">
			<!-- row -->
			<div class="row">
				<div class="col-md-12">
					<div class="section-title">
						<h2 class="title">
							<a href="/berita">Berita</a>
						</h2>
					</div>
				</div>
				{{-- POST --}}
				<div class="row form-group container-fluid">
					@foreach($berita as $dt)
					<div class="col-sm-4">
						<a class="post-img" href="/berita/detail/{{$dt->id}}">
							@if($dt->gambar != null || $dt->gambar != "")
							<div id="berita_img">
								<img src="public/uploads/{{$dt->gambar}}">
							</div>
							@endif
						</a><br>
						<h3>
							<a href="/berita/detail/{{$dt->id}}" style="color: #ee4266;">{{$dt->subject}}</a>
						</h3>
						<p>{!! str_limit($dt->isi, 200) !!}</p>
						<hr style="height:2px;border-width:0;color:gray;background-color:gray">
					</div>
					@endforeach
				</div>

				{{-- ARTIKEL --}}
				<div class="row">
					<div class="col-md-12">
						<div class="section-title">
							<h2 class="title">
								<a href="/artikel">Artikel</a>
							</h2>
						</div>
					</div>
					{{-- POST ARTIKEL --}}
					<div class="row form-group container-fluid">
						@foreach($artikel as $ar)
						<div class="col-md-4">
							<div class="post container-fluid">
								<a class="post-img" href="/artikel/detail/{{$ar->id}}">
									<img src="{{url('lembata/img/read.jpg')}}" alt="">
								</a>
								<div class="post-body">
									{{-- <div class="post-category"> --}}
										<h3>
											<a href="/artikel/detail/{{$ar->id}}" style="color: #ee4266;">{{$ar->judul}}</a>
										</h3>
									{{-- </div> --}}
									<h3 class="post-title"><a href="#"></a></h3>
									<ul class="post-meta">
										<p>{!! str_limit($ar->isi, 200) !!}</p>
										<hr style="height:2px;border-width:0;color:gray;background-color:gray">
									</ul>
								</div>
							</div>
						</div>
						@endforeach
					</div>
				</div>


				<!-- /post -->
			</div>

		</div>
	</div>
	<!-- /row -->
</div>
<!-- /container -->
@include('layout.footer')