
@include('layout.header')
<style type="text/css">
	.bs-example{
		margin: 40px;        
	}
</style>
<!-- container -->
<div class="container">
	<!-- row -->
	<!-- row -->
	<!-- post -->
	{{-- @foreach($artikel as $dt) --}}
	<div class="row form-group">



		{{-- @endforeach --}}
		{{-- @for($x=0; $x<5; $x++) --}}
		<h3>Info PPID</h3>
		<hr>
		<div class="container-fluid">
			<div class="text-center alert alert-success alert-block">
				<p>Untuk Mendapatkan Informasi Dan Dokumen, Silahkan Mengirimkan Email Ke Alamat
					<a href="mailto:ppid@kpulembata.com">ppid@kpulembata.com</a>
				</p>
				<p>Semua Bentuk Permohonan Informasi Dan Dokumen Yang Diminta Akan Dilayani Melalui Email Dan Dikirim Langsung Ke Email Pemohon.
				</p>
				<p>Dalam Waktu 1 x 24 Jam Hari Kejra Pemohon Sudah Bisa Menerima Balasan Untuk Informasi Yang Dimaksud.</p>
				<p>Berikut Daftar Permintaan yang sudah diminta pada PPID KPU Lembata :</p>
			</div>
			<table class="table table-hover">
				<thead>
					<tr>
						<th>No</th>
						<th>Tanggal</th>
						<th>Nama Pemohon</th>
						<th>Alamat</th>
						<th>Informasi Yg Diperlukan</th>
					</tr>
				</thead>
				<?php $no=1;?>
				@foreach($data as $dt)
				<tbody>
					<?php
					$ps = explode("-",$dt->tgl);
					$month = $ps[1];
					$dateObj   = DateTime::createFromFormat('!m', $month);
									$monthName = $dateObj->format('F'); // March
									$date = $ps[2]." ".$monthName." ".$ps[0];
									?>
									<td>{{$no++}}</td>
									<td>{{$date}}</td>
									<td>{{$dt->nama_pemohon}}</td>
									<td>{{$dt->alamat}}</td>
									<td>{{$dt->informasi}}</td>
								</tbody>
								@endforeach
							</table>
						</div>
					</div>
					{{-- @endfor --}}
					{{-- end foreach --}}
				</div>
				<!-- /container -->
				@include('layout.footer')