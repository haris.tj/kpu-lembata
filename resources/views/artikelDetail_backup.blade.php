@include('layout.header');
<div class="section">
	<!-- container -->
	<div class="container">
		<!-- row -->
		<div class="col-md-1"></div>
		<div class="row">
			<div class="col-md-10">
				<!-- row -->
				<div class="row">
					<div class="col-md-12">
						<div class="section-title">
							<h2 class="title">Artikel</h2>
						</div>
					</div>
					<!-- post -->

					<div class="container-fluid">
						@foreach($detail as $dt)
						<h3 style="color: #ee4266;">{{$dt->judul}}</h3>
						<p><small>Tanggal : {{$dt->tanggal}}</small></p><hr>
						<p>{!! $dt->isi !!}</p>
						@endforeach
					</div>
				</div>
				<h4>Artikel Lainnya<hr></h4>
				<!-- /post -->
				@foreach($detail2 as $dt2)
				<div class="col-md-4">
					<div class="post">
						<a class="post-img" href="/artikel/detail/{{$dt2->id}}"><img src="{{url('lembata/img/read.jpg')}}" alt=""></a>
						<div class="post-body">
							<div class="post-category">
								<a href="/artikel/detail/{{$dt2->id}}">{{$dt2->judul}}</a>
							</div>
							<h3 class="post-title"><a href="#"></a></h3>
							<ul class="post-meta">

								<li>{{$dt2->tanggal}}</li>
							</ul>
						</div>
					</div>
				</div>
				@endforeach
				<!-- post -->
			</div>
		</div>
		<div class="col-md-12"></div>
	</div>
	<!-- /row -->
</div>
@include('layout.footer');