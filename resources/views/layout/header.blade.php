<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	{{-- <meta name="viewport" content="width=device-width, initial-scale=1"> --}}
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

	<title>KPU Lembata</title>
	<link href='{{url('lembata/img/LogoKPU.png')}}' rel='shortcut icon'>
	<!-- Google font -->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700%7CMuli:400,700" rel="stylesheet">

	<!-- Bootstrap -->
	<link type="text/css" rel="stylesheet" href="{{url('lembata/css/bootstrap.min.css')}}" />

	<!-- Font Awesome Icon -->
	<link rel="stylesheet" href="{{url('lembata/css/font-awesome.min.css')}}">

	<!-- Custom stlylesheet -->
	<link type="text/css" rel="stylesheet" href="{{url('lembata/css/style.css')}}" />
	<link type="text/css" rel="stylesheet" href="{{url('lembata/css/massharis.css')}}" />
	<script src="https://use.fontawesome.com/e75629fc10.js"></script>
</head>
<body>
	<!-- HEADER -->
	<header id="header">
		<!-- NAV -->
		<div id="nav">
			<!-- Top Nav -->
			<div id="nav-top">
				<div class="container">
					<!-- social -->
					<ul class="nav-social">
						<li><a href="#"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
						<li><a href="#"><i class="fa fa-instagram"></i></a></li>
					</ul>
					<!-- /social -->

					<!-- logo -->
					<div class="nav-logo">
						<a href="/" class="logo">
							<img src="{{url('lembata/img/logoo.png')}}" alt="" class="img-responsive">
						</a>
					</div>
					<!-- /logo -->

					<!-- search & aside toggle -->
					<div class="nav-btns">
						<button class="aside-btn"><i class="fa fa-bars"></i></button>
						<button class="search-btn"><i class="fa fa-search"></i></button>
						<div id="nav-search">
							<form>
								<input class="input" name="search" placeholder="Enter your search...">
							</form>
							<button class="nav-close search-close">
								<span></span>
							</button>
						</div>
					</div>
					<!-- /search & aside toggle -->
				</div>
			</div>
			<!-- /Top Nav -->

			<!-- Main Nav -->
			<div id="nav-bottom">
				<div class="container">
					<!-- nav -->
					<ul class="nav-menu">
						<li class="has-dropdown">
							<a href="/">Home</a>
							<div class="dropdown">
								<div class="dropdown-body">
									<ul class="dropdown-list">
										<li><a href="/visi-misi">Visi & Misi</a></li>
										<li><a href="/komisioner">Komisioner KPU</a></li>
										<li><a href="/profil">Profil Sekertariat</a></li>
										<li><a href="/struktur-organisasi">Struktur Organisasi</a></li>
										{{-- <li><a href="#">Tentang Kami</a></li> --}}
									</ul>
								</div>
							</div>
						</li>
						

				{{-- 		<li class="has-dropdown">
							<a href="/">Data Pemilih</a>
							<div class="dropdown">
								<div class="dropdown-body">
									<ul class="dropdown-list">
										<li><a href="#">Data Pileg</a></li>
										<li><a href="#">Data Pilpres</a></li>
										<li><a href="#">Data Pilkada</a></li>
										
									</ul>
								</div>
							</div>
						</li> --}}
						<li class="has-dropdown">
							<a href="/">Publikasi</a>
							<div class="dropdown">
								<div class="dropdown-body">
									<ul class="dropdown-list">
										<li><a href="{{url('artikel')}}">Artikel</a></li>
										<li><a href="{{url('berita')}}">Berita</a></li>
										<li><a href="{{url('agenda')}}">Agenda</a></li>
										<li><a href="{{url('album')}}">Galeri Photo</a></li>
									</ul>
								</div>
							</div>
						</li>
						<li><a href="/data-pemilih">Data Pemilih</a></li>
						<li><a href="/produk-hukum">Produk Hukum</a></li>
						<li><a href="/info">Info PPID KPU Lembata</a></li>
						<li><a href="{{url('kontak')}}">Hubungi Kami</a></li>
					</ul>
					<!-- /nav -->
				</div>
			</div>
			<!-- /Main Nav -->

			<!-- Aside Nav -->
			<div id="nav-aside">
				<ul class="nav-aside-menu">
					<li class="has-dropdown"><a>Profil</a>
						<ul class="dropdown">
							{{-- <li><a href="about.html">Tentang Kami</a></li> --}}
							<li><a href="/visi-misi">Visi & Misi</a></li>
							<li><a href="/komisioner">Komisioner KPU</a></li>
							<li><a href="/profil">Profil Sekertariat</a></li>
							<li><a href="/struktur-organisasi">Struktur Organisasi</a></li>
							{{-- <li><a href="/kontak">Kontak KPU Lembata</a></li> --}}
							
						</ul>
					</li>
					<li><a href="/produk-hukum">Produk Hukum</a></li>
					<li class="has-dropdown"><a>Publikasi</a>
						<ul class="dropdown">
							<li><a href="{{url('artikel')}}">Artikel</a></li>
							<li><a href="{{url('berita')}}">Berita</a></li>
							<li><a href="{{url('agenda')}}">Agenda</a></li>
							<li><a href="{{url('album')}}">Galeri Photo</a></li>
							
						</ul>
					</li>
					<li><a href="/data-pemilih">Data Pemilih</a></li>
					<li><a href="/info">Info PPID KPU Lembata</a></li>
					<li><a href="{{url('kontak')}}">Hubungi Kami</a></li>
					
				</ul>
				<button class="nav-close nav-aside-close"><span></span></button>
			</div>
			<!-- /Aside Nav -->
		</div>
		<!-- /NAV -->

		<!-- PAGE HEADER -->

		<div class="row form-group">

			<div id="logo-container">
				<div class="col-sm-1"></div>
				<div class="col-sm-10">
					<img src="{{url('lembata/img/kpu.jpg')}}" alt="" class="responsive">
				</div>
				<div class="col-sm-1"></div>
			</div>

		</div>
		
		
		<!-- /PAGE HEADER -->
	</header>

