<!-- FOOTER -->
<footer id="footer">
	<!-- container -->
	<div class="container">
		<!-- row -->
		<div class="row form-group">
			<div class="col-md-5">
				<div class="footer-widget">
					<div class="footer-logo">

					</div>
					<h2><font color="white">Kontak</font></h2> 
					<h6 style="color: white">KPU Kabupaten Lembata</h6>
					<p>Jl. Trans Lembata, Lewoleba Tim., Nubatukan, Kabupaten Lembata, Nusa Tenggara Timur.</p>
					<p>Telp: (0383) 2343551</p>
					<p>E-mail: info@kpulembata.com</p>
					<ul class="contact-social">
						<li><a href="#" class="social-facebook"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#" class="social-twitter"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#" class="social-google-plus"><i class="fa fa-google-plus"></i></a></li>
						<li><a href="#" class="social-instagram"><i class="fa fa-instagram"></i></a></li>
					</ul>
				</div>
			</div>
			<div class="col-md-6 text-right">
				<div class="google-maps">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3947.4516106451542!2d123.45722781416099!3d-8.357155686525804!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dab0b609a5e2311%3A0xfbea07323047d289!2sKPU%20KABUPATEN%20LEMBATA!5e0!3m2!1sid!2sid!4v1597068262203!5m2!1sid!2sid" width="600" height="300" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
				</div>
			</div>
			
			
		</div>
		<!-- /row -->

		<!-- row -->
		<div class="footer-bottom row">
			<div class="col-md-6 col-md-push-6">
				<ul class="footer-nav">
					<p>kab-lembata.kpu.go.id</p>
				</ul>
			</div>
			<div class="col-md-6 col-md-pull-6">
				<div class="footer-copyright">
					<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
					Copyright &copy;<script>document.write(new Date().getFullYear());</script> KPU Kabupaten Lembata
					<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
				</div>
			</div>
		</div>
		<!-- /row -->
	</div>
	<!-- /container -->
</footer>
<!-- /FOOTER -->

<!-- jQuery Plugins -->
{{-- <script src="{{url('lembata/js/jquery.min.js')}}"></script> --}}
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script src="{{url('lembata/js/bootstrap.min.js')}}"></script>
<script src="{{url('lembata/js/jquery.stellar.min.js')}}"></script>
<script src="{{url('lembata/js/main.js')}}"></script>
@include('sweetalert::alert')
</body>
</html>
