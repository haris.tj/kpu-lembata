
@include('layout.header');
<!-- container -->
<div class="container">
	<!-- row -->
	<div class="row">
		<div class="col-md-12">
			<!-- row -->
			<div class="row">
				<div class="col-md-12">
					<div class="section-title">
						<h2 class="title">Artikel</h2>
					</div>
				</div>
				<!-- post -->
				@foreach($artikel as $dt)
				<div class="col-md-4">
					<div class="post container-fluid">
						<a class="post-img" href="/artikel/detail/{{$dt->id}}">
							<img src="{{url('lembata/img/read.jpg')}}" alt="">
						</a>
						<div class="post-body">
							<div class="post-category">
								<a href="/artikel/detail/{{$dt->id}}">{{$dt->judul}}</a>
							</div>
							<h3 class="post-title"><a href="#"></a></h3>
							<ul class="post-meta">

								<li>{{$dt->tanggal}}</li>
							</ul>
						</div>
					</div>
				</div>
				@endforeach
			</div>
			<div class="col-md-12">
				<div class="section-row loadmore text-right">
					{{ $artikel->links() }}
				</div>
			</div>
		</div>
	</div>
	<!-- /row -->
</div>
<!-- /container -->
@include('layout.footer');