@include('layout.header')
<!-- container -->
<style type="text/css">
	.pagination li{
		list-style-type: none;
		margin:5px;
	}
</style>
<div class="container">
	<!-- row -->
	<div class="row">
		<div class="col-md-12">
			<!-- row -->
			<div class="row">
				<div class="col-md-12">
					<div class="section-title">
						<h2 class="title">Berita</h2>
					</div>
				</div>
				{{-- POST --}}
				<div class="row form-group container-fluid">
					@foreach($artikel as $dt)
					<div class="col-sm-4">
						<a class="post-img" href="/berita/detail/{{$dt->id}}">
							@if($dt->gambar != null || $dt->gambar != "")
							<div id="berita_img">
								<img src="public/uploads/{{$dt->gambar}}">
							</div>
							@endif
						</a><br>
						<h3>
							<a href="/berita/detail/{{$dt->id}}" style="color: #ee4266;">{{$dt->subject}}</a>
						</h3>
						<p>{!! str_limit($dt->isi, 200) !!}</p>
						<hr style="height:2px;border-width:0;color:gray;background-color:gray">
					</div>
					@endforeach
				</div>

				<div class="col-md-12">
					<div class="section-row loadmore text-right">
						{{ $artikel->links() }}
					</div>
				</div>

				<!-- /post -->
			</div>

		</div>
	</div>
	<!-- /row -->
</div>
<!-- /container -->
@include('layout.footer')