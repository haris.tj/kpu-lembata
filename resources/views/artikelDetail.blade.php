@include('layout.header')
<div class="section">
	<!-- container -->
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<h3>Artikel</h3><hr>
			</div>
			<div class="col-md-1"></div>
		</div>

		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-7">
				@foreach($detail as $dt)
				<h3 style="color: #ee4266;">{{$dt->judul}}</h3>
				<p><small>Tanggal : {{$dt->tanggal}}</small></p><hr>
				<p>{!! $dt->isi !!}</p>
				@endforeach
			</div>
			<h3>Artikel Lainnya</h3>
			{{-- @for($x=1; $x<4; $x++) --}}
			@foreach($detail2 as $dt2)
			<div class="col-md-3">
				<h3>
					<a href="/artikel/detail/{{$dt2->id}}" style="color: #ee4266;">{{$dt2->judul}}</a>
				</h3>
				<p><small>Tanggal : {{$dt->tanggal}}</small></p>
				<div class="tab-content">
					<p>{!! str_limit($dt->isi, 200) !!}</p>
				</div>
			</div>
			@endforeach
			<div class="col-md-1"></div>
		</div>
		{{-- komen with fb --}}
		<div class="row form-group">
			<hr>
			<div class="col-sm-1"></div>
			<div class="col-sm-10 text-center">
				<div class="fb-comments" data-href="{{Request::url()}}" data-numposts="5" data-width=""></div>
			</div>
			<div class="col-sm-1"></div>
		</div>
		<div id="fb-root"></div>
		<script async defer crossorigin="anonymous" src="https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v8.0&appId=326289295390633&autoLogAppEvents=1" nonce="k0AG1fbj"></script>

	</div>
</div>
<!-- /row -->
</div>
@include('layout.footer')