
@include('layout.header');
<!-- container -->
<div class="container">
	<!-- row -->
	<div class="row">
		<div class="col-md-12">
			<!-- row -->
			<div class="row">
				<div class="col-md-12">
					<div class="section-title">
						<h2 class="title">Agenda</h2>
					</div>
				</div>
				<!-- post -->
				<form>
					<div class="container-fluid text-center">
						<div class="row form-group">
							<div class="col-xs-1"></div>
							<div class="col-xs-5">
								<label class="form-group">Bulan</label>
								<select name="bulan" class="form-control" id="bulan">
									<option value="1">Januari</option>
									<option value="2">Februari</option>
									<option value="3">Maret</option>
									<option value="4">April</option>
									<option value="5">Mei</option>
									<option value="6">Juni</option>
									<option value="7">Juni</option>
									<option value="8">Agustus</option>
									<option value="9">September</option>
									<option value="10">Oktober</option>
									<option value="11">November</option>
									<option value="12">Desember</option>
								</select>
							</div>
							<div class="col-xs-5">
								<label class="form-group">Tahun</label>
								<input type="number" name="tahun" placeholder="Tahun" required="" id="tahun" value="{{date('Y')}}" class="form-control"><br>
							</div>
							{{-- <a href="#"class="btn btn-sm btn-round btn-primary" type="submit" id="submit" name="submit">Agenda</a> --}}
							<div class="col-xs-1"></div>
						</div>
					</div>


				</form>
				<div class="col-sm-12">
					<div class="container-fluid">
						<h3>Data Agenda</h3>         
						<table class="table table-hover">
							<thead>
								<tr>
									<th>No</th>
									<th>Tanggal</th>
									<th>Acara</th>
									<th>Kagiatan</th>
								</tr>
							</thead>
							<tbody id="show_data">
								{{--  --}}
							</tbody>
						</table>
					</div>
				</div>

			</div>
		</div>
	</div>
	<!-- /row -->
</div>
<!-- /container -->
@include('layout.footer');
<script type="text/javascript">
	$(document).ready(function() {
		// $('#submit').click(function(event) {
		// 	tampildata();
		// });

		$('#bulan').change(function(event) {
			tampildata();
		});

		$('#tahun').change(function(event) {
			tampildata();
		});

		function tampildata(){
			var x = document.getElementById('bulan').value;
			var y = document.getElementById('tahun').value;
			var a = "{{url('/agenda/')}}";
			var url = a+'/'+x+'/'+y;

			$.ajax({
				type  : 'GET',
				url   : url,
				async : false,
				dataType : 'json',
				success : function(data){
					var html = '';
					var i;
					var no = 1;
					for(i=0; i<data.length; i++){
						var cnv = x - 1;
						var z = data[i].tanggal;
						var date = new Date(y, cnv, z);  
						var month = date.toLocaleString('default', { month: 'long' });
						var tgl = z+" "+month+" "+y;
						var aaa = no++;

						html += '<tr>'+
						'<td>'+aaa+'</td>'+
						'<td>'+tgl+'</td>'+
						'<td>'+data[i].acara+'</td>'+
						'<td>'+data[i].kegiatan+'</td>'+
						'</tr>';
					}
					$('#show_data').html(html);
				}

			});

		}
	});
</script>