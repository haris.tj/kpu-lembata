
@include('layout.header')
<div class="container">
	<div class="row">
		<h3>Kritik & Saran</h3>
		<hr style="height:2px;border-width:0;color:gray;background-color:gray">
	</div>
</div>

<div class="container">
	<div class="row-fluid form-group">
		<table class="table table-hover">
			<thead>
				<tr>
					<th>No</th>
					<th>Nama</th>
					<th>Email</th>
					<th>Alamat</th>
					<th>Kritik & Saran</th>
				</tr>
			</thead>
			<?php $no=1;?>
			@foreach($data as $dt)
			<tbody>
				<td>{{$no++}}</td>
				<td>{{$dt->nama}}</td>
				<td>{{$dt->email}}</td>
				<td>{{$dt->alamat}}</td>
				<td>{{$dt->pesan}}</td>
			</tbody>
			@endforeach
		</table>
	</div>
	{{ $data->links() }}
</div>
@include('layout.footer')