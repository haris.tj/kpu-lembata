@include('layout.header')
<div class="section">

	<div class="container-fluid">

		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<h3>Berita</h3><hr>
			</div>
			<div class="col-md-1"></div>
		</div>

		<div class="row">
			<div class="col-md-1"></div>

			<div class="col-md-7">
				@foreach($detail as $dt)
				<h3 style="color: #ee4266;">{{$dt->subject}}</h3>
				<p><small>Tanggal : {{$dt->postdate}}</small></p><hr>
				@if($dt->gambar != null || $dt->gambar != "")
				<div id="berita_img" class="text-center">
					<img src="{{url('public/uploads/'.$dt->gambar)}}" alt="">
				</div>
				@endif
				<p>{!! $dt->isi !!}</p>
				@endforeach
			</div>
			
			<h3>Berita Lainnya</h3>
			@foreach($detail2 as $dt2)
			<div class="col-md-3">
				<h3>
					<a href="/berita/detail/{{$dt2->id}}" style="color: #ee4266;">{{$dt2->subject}}</a>
				</h3>
				<div id="berita_img" class="text-center">
					<img src="{{url('public/uploads/'.$dt2->gambar)}}" alt="">
				</div>
				<p><small>Tanggal : {{$dt2->postdate}}</small></p>
				<div class="tab-content">
					<p>{!! str_limit($dt2->isi, 200) !!}</p>
				</div>
			</div>
			@endforeach
			
			<div class="com-md-1"></div>
		</div>
		{{-- FB comment --}}
		<div class="row form-group">
			<hr>
			<div class="col-sm-1"></div>
			<div class="col-sm-10 text-center">
				<div class="fb-comments" data-href="{{Request::url()}}" data-numposts="5" data-width=""></div>
			</div>
			<div class="col-sm-1"></div>
		</div>
		<div id="fb-root"></div>
		<script async defer crossorigin="anonymous" src="https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v8.0&appId=326289295390633&autoLogAppEvents=1" nonce="k0AG1fbj"></script>

	</div> 

</div>
@include('layout.footer')
