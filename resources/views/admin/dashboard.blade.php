<div class="row">
  <!-- Berita -->
  <div class="col-md-4 col-sm-6 col-xs-6">           
    <div class="panel panel-back noti-box">
      <span class="icon-box bg-color-red set-icon">
        <i class="fas fa-user-secret fa-6x"></i>
      </span>
      <div class="text-box" >
       <p class="main-text"><h3 style="text-align: center;"><strong><?php echo $user ?></strong></h3></p>
       <p class="text-muted"style="text-align: center;"><a href="#"><strong>User</strong></a></p>
     </div>
   </div>
 </div>

 <!-- Produk -->
 <div class="col-md-4 col-sm-6 col-xs-6">           
  <div class="panel panel-back noti-box">
    <span class="icon-box bg-color-red set-icon">
      <i class="fas fa-newspaper fa-6x"></i>
    </span>
    <div class="text-box" >
      <p class="main-text"><h3 style="text-align: center;"><strong>#test</strong></h3></p>
      <p class="text-muted" style="text-align: center;"><a href="#"><strong>Pengumuman</strong></a></p>
    </div>
  </div>
</div>

<!-- Video -->
<div class="col-md-4 col-sm-6 col-xs-6">           
  <div class="panel panel-back noti-box">
    <span class="icon-box bg-color-red set-icon">
      <i class="far fa-bookmark fa-6x"></i>
    </span>
    <div class="text-box" >
     <p class="main-text"><h3 style="text-align: center;"><strong></strong></h3></p>
     <p class="text-muted"style="text-align: center;"><a href="#"><strong>Jadwal</strong></a></p>
   </div>
 </div>
</div>
</div>