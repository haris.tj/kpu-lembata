   @if(empty(Session::get('userid')))
        <script type="text/javascript">
            window.location = "/admin/login";//here double curly bracket
        </script>
    @endif
<div class="footer">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
				Copyright &copy; <script>document.write(new Date().getFullYear())</script> . Dashboard by <a href="#">MassHaris</a>.
			</div>
		</div>
	</div>
</div>
<!-- ============================================================== -->
<!-- end footer -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- end main wrapper  -->
<!-- ============================================================== -->
<!-- Optional JavaScript -->
<!-- jquery 3.3.1 -->
@include('sweetalert::alert')

<script src="{{url('assetz/vendor/jquery/jquery-3.3.1.min.js')}}"></script>
<!-- bootstap bundle js -->
<script src="{{url('assetz/vendor/bootstrap/js/bootstrap.bundle.js')}}"></script>
<!-- slimscroll js -->
<script src="{{url('assetz/vendor/slimscroll/jquery.slimscroll.js')}}"></script>
<!-- main js -->
<script src="{{url('assetz/libs/js/main-js.js')}}"></script>
<!-- chart chartist js -->
<script src="{{url('assetz/vendor/charts/chartist-bundle/chartist.min.js')}}"></script>
<!-- sparkline js -->
<script src="{{url('assetz/vendor/charts/sparkline/jquery.sparkline.js')}}"></script>
<!-- morris js -->
<script src="{{url('assetz/vendor/charts/morris-bundle/raphael.min.js')}}"></script>
<script src="{{url('assetz/vendor/charts/morris-bundle/morris.js')}}"></script>
<!-- chart c3 js -->
<script src="{{url('assetz/vendor/charts/c3charts/c3.min.js')}}"></script>
<script src="{{url('assetz/vendor/charts/c3charts/d3-5.4.0.min.js')}}"></script>
<script src="{{url('assetz/vendor/charts/c3charts/C3chartjs.js')}}"></script>
<script src="{{url('assetz/libs/js/dashboard-ecommerce.js')}}"></script>

<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script>
	$(document).ready(function () {
		$('#datatable').dataTable();
	});
</script>
</body>

</html>