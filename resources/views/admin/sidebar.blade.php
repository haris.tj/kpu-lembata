   @if(empty(Session::get('userid')))
   <script type="text/javascript">
            window.location = "/admin/login";//here double curly bracket
        </script>
        @endif
        <div class="nav-left-sidebar sidebar-dark">
            <div class="menu-list">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="d-xl-none d-lg-none" href="#">Dashboard</a>
                    <button class="navbar-toggler" type="button" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav flex-column">
                            <li class="nav-divider">
                                Menu
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link" href="#" ><i class="fa fa-fw fa-user-circle"></i>Dashboard <span class="badge badge-success">6</span></a>
                          {{--       <li class="nav-item">
                                    <a class="nav-link" href="#"><i class="fas fa-newspaper"></i>Pengumuman</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#"><i class="fa fa-fw fa-rocket"></i>Keterangan</a>
                                </li> --}}
                                {{-- PUBLIKASI --}}
                                <li class="nav-item">
                                    <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-5" aria-controls="submenu-5"><i class="fas fa-fw fa-table"></i>Publikasi</a>
                                    <div id="submenu-5" class="collapse submenu" style="">
                                        <ul class="nav flex-column">
                                            <li class="nav-item">
                                                <a class="nav-link" href="{{url('admin/artikel')}}">Artikel</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="/admin/berita">Berita</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="{{url('admin/agenda')}}">Agenda</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="{{url('admin/album')}}">Galery Photo</a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                {{-- HOME --}}
                                <li class="nav-item">
                                    <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-6" aria-controls="submenu-6"><i class="fas fa-fw fa-table"></i>Home</a>
                                    <div id="submenu-6" class="collapse submenu" style="">
                                        <ul class="nav flex-column">
                                            <li class="nav-item">
                                                <a class="nav-link" href="{{url('admin/visi-misi')}}">Visi Misi</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="{{url('admin/komisioner')}}">Komisioner KPU</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="{{url('admin/user/home')}}">Profil Sekertariat</a>
                                            </li>
                                            {{-- <li class="nav-item">
                                                <a class="nav-link" href="#">Kontak KPU Lembata</a>
                                            </li> --}}
                                        </ul>
                                    </div>
                                </li>
                                {{--  --}}
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('admin/hukum')}}"><i class="fa fa-fw fa-rocket"></i>Produk Hukum</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('admin/info')}}"><i class="fa fa-fw fa-rocket"></i>Info PPID</a>
                                </li>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>