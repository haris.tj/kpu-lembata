
@include('layout.header');
<style type="text/css">
	.bs-example{
		margin: 40px;        
	}
</style>
<!-- container -->
<div class="container">
	<!-- row -->
	<div class="row">
		<div class="col-md-12">
			<!-- row -->
			<div class="row form-group">
				<!-- post -->
				{{-- @foreach($artikel as $dt) --}}
				<div class="row form-group">
					<div class="col-sm-8">
						<h2>Data Pemilih</h2>
					</div>
					<div class="col-sm-4 text-right">
						<select class="form-control" name="nama" id="nama">
							<option>Pilih</option>
							<option value="DATA PEMILIH PILEG">DATA PEMILIH PILEG</option>
							<option value="DATA PEMILIH PILPRES">DATA PEMILIH PILPRES</option>
							<option value="DATA PEMILIH PILKADA">DATA PEMILIH PILKADA</option>
						</select>
					</div>
					{{-- <div class="col-sm-8"></div> --}}
				</div>
				<div class="col-sm-12">
					<hr style="height:2px;border-width:0;color:gray;background-color:gray">
				</div>
				{{-- @endforeach --}}
				{{-- @for($x=0; $x<5; $x++) --}}
				<div id="show">
					{{-- data tampil disini --}}
				</div>
				{{-- @endfor --}}
				{{-- end foreach --}}
			</div>
		</div>
	</div>
	<!-- /row -->
</div>
<!-- /container -->
@include('layout.footer');
<script type="text/javascript">
	$(document).ready(function() {
		$('#nama').change(function(event) {

			var id = document.getElementById('nama').value;
			var p = "{{url('/data-pemilih/')}}";
			var url = p+"/"+id;
			// alert(url);

			$.ajax({
				type  : 'GET',
				url   : url,
				async : false,
				dataType : 'json',
				success : function(data){
					var htmlx = '';
					var i;
					for(i=0; i<data.length; i++){
						var dwnld = "{{url('public/uploads/data-pemilih/')}}";
						var file = data[i].file;
						var downloadlink = dwnld+"/"+file;
						var waktu = data[i].tgl;
						var spl = waktu.split("-");

						var bln = spl[1] - 1;

						const date = new Date(spl[0], bln, spl[1]);  // 2009-11-10
						const month = date.toLocaleString('default', { month: 'long' });
						var cnv = spl[2]+" "+month+" "+spl[0];
					
						htmlx +=
						'<div class="col-sm-1"></div>'+
						'<div class="col-sm-10">'+
						'<div class="card">'+
						'<div class="card-header">'+
						'<h4>'+data[i].judul+'</h4>'+
						'<p><small>'+cnv+'</small></p>'+
						'</div>'+
						'<div class="card-body">'+
						data[i].keterangan
						+'</div>'+
						'<div class="card-footer text-right">'+
						'<a href="'+downloadlink+'" target="_blank" class="btn btn-rounded btn-sm btn-primary">Download</a>'+
						'</div>'+
						'<hr>'+
						'</div>'+
						'</div>'+
						'<div class="col-sm-12"></div>';

					}
					$('#show').html(htmlx);
				}

			});
		});
	});
</script>