
@include('layout.header')
<!-- container -->
<div class="container">
	<div class="row form-group">
		<div class="col-sm-6">
			<div class="card">
				<div class="container-fluid">
					<br>
					<h2>Kontak</h2> 
					<h6>KPU Kabupaten Lembata</h6>
					<p>Jl. Trans Lembata, Lewoleba Tim., Nubatukan, Kabupaten Lembata, Nusa Tenggara Timur.</p>
					<p>Telp: (0383) 2343551</p>
					<p>E-mail: info@kpulembata.com</p>
					<ul class="contact-social">
						<li><a href="#" class="social-facebook"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#" class="social-twitter"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#" class="social-google-plus"><i class="fa fa-google-plus"></i></a></li>
						<li><a href="#" class="social-instagram"><i class="fa fa-instagram"></i></a></li>
					</ul><br>
				</div>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="card">
				<div class="container-fluid">
					<br><h2>Kritik & Saran</h2>
					<form action="/kritik-saran/send" method="post" enctype="multipart/form-data" role="form">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<div class="row form-group">
							<div class="col-xs-6">
								<label>Nama</label>
								<input type="text" name="nama" class="form-control" required>
							</div>
							<div class="col-xs-6">
								<label>Email</label>
								<input type="email" name="email" class="form-control" required>
							</div>
						</div> 
						<div class="row form-group">
							<div class="col-xs-12">
								<label>Alamat</label>
								<input type="text" name="alamat" class="form-control" required>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-xs-12">
								<label>Pesan</label>
								<textarea class="form-control" id="message-text" name="pesan" required></textarea>
							</div>
						</div> 
						<div class="row form-group">
							<div class="container-fluid">
								<button type="submit" class="btn btn-primary">Send</button>
								<a href="/kritik-saran" class="btn btn-primary">Lihat Kritik & Saran</a>	
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /container -->
@include('layout.footer')