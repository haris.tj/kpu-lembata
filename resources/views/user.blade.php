@include('layout.header')
<!-- container -->
<div class="container">
	<div class="row form-group">
		{{-- <div class="col-sm-1"></div> --}}
		{{-- <div class="col-sm-10"> --}}
			<h3>Profil Sekertariat</h3>
			<div class="row">
				<div class="col-xs-4">
					<select class="form-control" name="jabatan" id="jabatan">
						<option active>Pilih</option>
						<option value="Sekertaris">Sekertaris</option>
						<option value="Sub Bagian Hukum">Sub Bagian Hukum</option>
						<option value="Sub Bagian Teknik Dan Hupmas">Sub Bagian Teknik Dan Hupmas</option>
						<option value="Sub Bagian Program Dan Data">Sub Bagian Program Dan Data</option>
						<option value="Sub Bagian Umum">Sub Bagian Umum</option>
					</select>
				</div>
			</div>
			<hr style="height:2px;border-width:0;color:gray;background-color:gray">
		</div>
		{{-- <div class="col-sm-1"></div> --}}
	{{-- </div> --}}
</div>


<div class="container">
	<div class="row-fluid form-group">
		<div id="show">
			{{-- SHOW DATA DISINI --}}
		</div>
	</div>
</div>

<!-- /container -->
@include('layout.footer')
<script type="text/javascript">
	$(document).ready(function() {
		$('#jabatan').change(function(event) {

			var id = document.getElementById('jabatan').value;
			var p = "{{url('/profil/')}}";
			var url = p+"/"+id;
			// alert(url);

			$.ajax({
				type  : 'GET',
				url   : url,
				async : false,
				dataType : 'json',
				success : function(data){
					var bio = '';
					var img = '';
					var i;

					for(i=0; i<data.length; i++){
						var cek_img = data[i].gambar;
						
						if (cek_img == null || cek_img == "") {
							cek_img = "public/uploads/super_man.png";
						}else{
							cek_img = "public/uploads/"+cek_img;
						}

						bio +=
						'<div class="col-md-4">'+
						'<div class="image-wrap">'+
						'<div class="image-popup">'+
						'<p><small>'+
						'Nama    : '+data[i].nama+'</br>'+
						'NIP     : '+data[i].nip+'</br>'+
						'TTL  	 : '+data[i].tempat_lahir+'- '+data[i].ttl+ '</br>'+
						'Jabatan : '+data[i].jabatan+'</br>'+
						'Alamat  : '+data[i].alamat+'</br>'+
						'Email	 : '+data[i].email+'</br>'+
						'Telepon : '+data[i].telepon+'</small></p><hr>'+
						'</div>'+
						'<img src="'+cek_img+'" height="380" width="347" class="rounded-corners">'+
						'</div>'+
						'</div>';
					}
					$('#show').html(bio);
				}

			});
		});
	});
</script>