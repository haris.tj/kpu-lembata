@include('admin.header');
@include('admin.sidebar');
<div class="dashboard-wrapper">
	<div class="dashboard-ecommerce">
		<div class="container-fluid dashboard-content ">
			<h4 class="title"> <strong>Data Berita</strong></h4>
			{{-- Notifikasi DISINI NANTINYA--}}
			@if ($message = Session::get('success'))
			<div class="alert alert-success alert-block">
				<button type="button" class="close" data-dismiss="alert">×</button> 
				<strong>{{ $message }}</strong>
			</div>
			@endif

			@if ($message = Session::get('error'))
			<div class="alert alert-danger alert-block">
				<button type="button" class="close" data-dismiss="alert">×</button> 
				<strong>{{ $message }}</strong>
			</div>
			@endif
			{{-- END OF NOTIFICATION --}}
			<div class="card">
				<div class="header">
					<a href="{{url('/admin/berita/insert')}}" class="btn btn-info btn-fill btn-sm fas fa-address-card"> Add</a>
				</div>
				<br>
				<div class="content table-responsive table-full-width">
					<table class="table table-hover table-striped" id="datatable">
						<thead>
							<th>No</th>
							<th>Judul</th>
							<th>Berita</th>
							<th>Tgl Post</th>
							<th>Action</th>
						</thead>
						<tbody>
							<?php $no = 1;?>
							@foreach($berita as $dt)
							<tr>
								<td>{{ $no++ }}</td>
								<td>{{ str_limit($dt->subject, 20)}}</td>
								<td>{!! str_limit($dt->isi, 120) !!} </td>
								<td>{{ $dt->postdate }}</td>
								<td>
									<a href="/admin/berita/detail/{{$dt->id}}" class="btn btn-primary btn-fill btn-sm">
										<i class="fa fa-info" aria-hidden="true"></i>
									</a>
									<a href="/admin/berita/{{$dt->id}}" class="btn btn-warning btn-fill btn-sm">
										<i class="fa fa-text-width" style="color: white" aria-hidden="true"></i>
									</a>
									{{-- LOAD MODAL FROM VIEWADMIN.DELETE --}}
									@include('view_admin.berita.Delete')
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	@include('admin.footer');