 @include('new_layout.header')
 @include('new_layout.sidebar')
 <div class="dashboard-wrapper">
 	<div class="dashboard-ecommerce">
 		<div class="container-fluid dashboard-content ">
 			<!-- ============================================================== -->
 			<!-- pageheader  -->
 			<!-- ============================================================== -->
 			@foreach($detail as $dt)
 			<div class="card">
 				<h5 class="card-header">{{$dt->subject}}</h5>
 				<div class="container-fluid">
 					<?php
 					$ps = explode("-",$dt->postdate);
 					$month = $ps[1];
 					$dateObj   = DateTime::createFromFormat('!m', $month);
									$monthName = $dateObj->format('F'); // March
									$date = $ps[2]." ".$monthName." ".$ps[0];
									?>

									{{-- <p>Tanggal : {{$dt->postdate}}</p> --}}
									<p class="card-text">
										<small class="text-muted">Tgl Post : {{ $date." ".$dt->posttime }}</small><br>
										<small>Visit : {{$dt->visits}}</small>
									</p>
									
								</div>
								<div class="card-body">
									<div class="row form-group">
										<div class="col-sm-2"></div>
										<div class="col-sm-8">
											<img class="card-img-top" src="/public/uploads/{{$dt->gambar}}" alt="Card image cap">
										</div>
									</div>
									<p>{!! $dt->isi !!}</p>
								</div>
								@endforeach
							</div>
							{{-- <a href="/admin/berita" class="btn-rounded btn-sm btn-info">Kembali</a> --}}
							<button class="btn-rounded btn-info btn-sm" onclick="kembali()">Kembali</button>
						</div>
					</div>
					<!-- ============================================================== -->
					<!-- END OF WRAPPER KONTEN -->
					<!-- ============================================================== -->    
					<!-- ============================================================== -->
					<!-- footer -->
					<!-- ============================================================== -->
					@include('new_layout.footer')
					<script type="text/javascript">
						function kembali() {
							window.location = "/admin/berita";
						}
					</script>