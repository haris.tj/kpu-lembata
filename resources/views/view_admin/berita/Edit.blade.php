   @include('new_layout.header');
   @include('new_layout.sidebar');
   <div class="dashboard-wrapper">
   	<div class="dashboard-ecommerce">
   		<div class="container-fluid dashboard-content ">
   			<form action="/admin/berita/update" method="post" enctype="multipart/form-data" role="form">
   				<input type="hidden" name="_token" value="{{ csrf_token() }}">
   			{{-- 	<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.0.15/tinymce.min.js" referrerpolicy="origin"></script>
   				<script type="text/javascript">
   					tinymce.init({selector:'textarea'});
   				</script> --}}
               @foreach($artikel as $dt)
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                 @if ($message = Session::get('error'))
                 <div class="alert alert-danger alert-block">
                  <button type="button" class="close" data-dismiss="alert">×</button> 
                  <strong>{{ $message }}</strong>
               </div>
               @endif
               <div class="card">
                  <h5 class="card-header">Update Berita</h5>
                  <div class="card-body">
                   <form class="needs-validation" novalidate>
                    <div class="row">
                     <input type="hidden" name="id" id="id" value="{{$dt->id}}">
                     <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
                      <label for="validationCustom01">Judul Berita</label>
                      <input type="text" class="form-control" id="judul" value="{{ $dt->subject }}" name="judul" placeholder="Judul Berita" required>
                      <div class="valid-feedback">
                       Looks good!
                    </div>
                 </div>
                 <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 ">
                  <label for="validationCustom01">Gambar Berita</label>
                  <input type="file" class="form-control" id="gambar" value="{{ $dt->gambar }}" name="gambar" placeholder="Gambar Berita">
                  <div class="valid-feedback">
                     Looks good!
                  </div>
               </div>
               <div class="col-md-12">
                 <div class="form-group">
                   <label>Berita</label>
                   <textarea name="konten" id="konten" class="ckeditor" rows="15" placeholder="Konten Berita">{{$dt->isi}}</textarea>
                </div>
             </div>
             @endforeach
          </div>
          <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 ">
            <button class="btn-sm btn-rounded btn-primary" type="submit">Simpan</button>
         </div>
      </div>
   </form>
</div>
</div>
</div>
</form>
@include('new_layout.footer');
</div>
</div>