{{-- <a href="#" class="btn btn-warning btn-fill btn-sm" data-toggle="modal" data-target="#updateInfr{{$dt->id_info}}">
	<i class="fa fa-text-width" style="color: white" aria-hidden="true"></i>
</a>  --}}
<button class="btn-rounded btn-xs btn-warning" data-toggle="modal" data-target="#updateInfr{{$dt->id_info}}">
	<i class="ti-pencil"></i>
</button>
<div class="modal fade" id="updateInfr{{$dt->id_info}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5>Update Info</h5>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times</button>
			</div>
			<form action="/admin/info/update" method="post" enctype="multipart/form-data" role="form">
				{{-- @foreach($data as $dt) --}}
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="id" value="{{$dt->id_info}}" id="id">
				{{method_field('patch')}}
				<div class="modal-body">
					<div class="row form-group">
						<div class="col-md-6">
							<label for="validationCustom01">Tanggal</label>
							<input type="date" name="tgl" class="form-control" value="{{$dt->tgl}}">
						</div>

						<div class="col-md-6">
							<label for="validationCustom01">Nama Pemohon</label>
							<input type="text" name="nama_pemohon" class="form-control" value="{{$dt->nama_pemohon}}">
						</div>
					</div>
					<div class="row form-group">
						<div class="col-sm-12">
							<label class="validationCustom01">Alamat</label>
							<input type="text" name="alamat" class="form-control" value="{{$dt->alamat}}">
						</div>
					</div>
					<div class="row form-group">
						<div class="col-sm-12">
							<label class="validationCustom01">Informasi yg diperlukan</label>
							<textarea class="form-control" name="informasi">{{$dt->informasi}}</textarea>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn-rounded btn-sm btn-info">Save</button>
					<button type="button" class="btn-rounded btn-sm btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>