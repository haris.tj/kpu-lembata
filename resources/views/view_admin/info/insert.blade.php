{{-- <a href="#" class="btn btn-info btn-fill btn-sm fas fa-address-card" data-toggle="modal" data-target="#insertInfo"> Add</a>  --}}
<button class="btn-rounded btn-sm btn-primary" data-toggle="modal" data-target="#insertInfo">
	<i class="ti-plus"> Tambah</i>
</button>
<div class="modal fade" id="insertInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5>Insert Info</h5>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times</button>
			</div>
			<form action="/admin/info/insert/act" method="post" enctype="multipart/form-data" role="form">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="id" value="" id="id">
				{{method_field('patch')}}
				<div class="modal-body">
					<div class="row form-group">
						<div class="col-md-6">
							<label for="validationCustom01">Tanggal</label>
							<input type="date" name="tgl" class="form-control">
						</div>

						<div class="col-md-6">
							<label for="validationCustom01">Nama Pemohon</label>
							<input type="text" name="nama_pemohon" class="form-control">
						</div>					</div>
						<div class="row form-group">
							<div class="col-sm-12">
								<label class="validationCustom01">Alamat</label>
								<input type="text" name="alamat" class="form-control">
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-12">
								<label class="validationCustom01">Informasi yg diperlukan</label>
								<textarea class="form-control" name="informasi"></textarea>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn-rounded btn-sm btn-info">Save</button>
						<button type="button" class="btn-rounded btn-sm btn-default" data-dismiss="modal">Close</button>
					</div>
				</form>
			</div>
		</div>
	</div>