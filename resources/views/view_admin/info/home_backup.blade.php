@include('admin.header');
@include('admin.sidebar');
<div class="dashboard-wrapper">
	<div class="dashboard-ecommerce">
		<div class="container-fluid dashboard-content ">
			<h4 class="title"> <strong>INFO PPID</strong></h4>
			{{-- Notifikasi DISINI NANTINYA--}}
			@if ($message = Session::get('success'))
			<div class="alert alert-success alert-block">
				<button type="button" class="close" data-dismiss="alert">×</button> 
				<strong>{{ $message }}</strong>
			</div>
			@endif

			@if ($message = Session::get('error'))
			<div class="alert alert-danger alert-block">
				<button type="button" class="close" data-dismiss="alert">×</button> 
				<strong>{{ $message }}</strong>
			</div>
			@endif
			{{-- END OF NOTIFICATION --}}
			<div class="card">
				<div class="header">
					{{-- <a href="{{url('/admin/hukum/insert')}}" class="btn btn-info btn-fill btn-sm fas fa-address-card"> Add</a> --}}
					@include('view_admin/info/insert')
				</div>
				<br>
				<div class="content table-responsive table-full-width">
					<table class="table table-hover table-striped" id="datatable">
						<thead>
							<th>No</th>
							<th>Tanggal</th>
							<th>Nama Pemohon</th>
							<th>Alamat</th>
							<th>Informasi</th>
							<th>Action</th>
						</thead>
						<tbody>
							<?php $no = 1;?>
							@foreach($data as $dt)
							<tr>
								<td>{{ $no++ }}</td>
								<td>{{$dt->tgl}}</td>
								<td>{{$dt->nama_pemohon}} </td>
								<td>{{$dt->alamat}}</td>
								<td>{{$dt->informasi}}</td>
								<td>
									@include('view_admin.info.update')
									@include('view_admin.info.delete')
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	@include('admin.footer');