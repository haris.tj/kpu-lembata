@include('new_layout.header')
@include('new_layout.sidebar')
<div class="dashboard-wrapper">
	<div class="container-fluid dashboard-content">
		<!-- ============================================================== -->
		<!-- pageheader -->
		<!-- ============================================================== -->
		<div class="row">
			<!-- ============================================================== -->
			<!-- data table multiselects  -->
			<!-- ============================================================== -->
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				{{-- Notifikasi DISINI NANTINYA--}}
				
				{{-- END OF NOTIFICATION --}}
				<div class="card">
					<div class="card-header">
						<div class="row form-group">
							<div class="col-sm-6">
								@include('view_admin/info/insert')
							</div>
							<div class="col-sm-6 text-right">
								<h2>INFO PPID</h2>
							</div>
						</div>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="example3" class="table table-striped table-bordered" style="width:100%">
								<thead>
									<th>No</th>
									<th>Tanggal</th>
									<th>Nama Pemohon</th>
									<th>Alamat</th>
									<th>Informasi</th>
									<th>Action</th>
								</thead>
								<tbody>
									<?php $no = 1;?>
									@foreach($data as $dt)
									<tr>
										<td>{{ $no++ }}</td>
										<td>{{$dt->tgl}}</td>
										<td>{{$dt->nama_pemohon}} </td>
										<td>{{$dt->alamat}}</td>
										<td>{{$dt->informasi}}</td>
										<td>
											@include('view_admin.info.update')
											@include('view_admin.info.delete')
										</td>
									</tr>
									@endforeach
								</table>
							</div>
						</div>
					</div>
				</div>
				<!-- ============================================================== -->
				<!-- end data table multiselects  -->
				<!-- ============================================================== -->
			</div>
			<!-- ============================================================== -->
			<!-- end pageheader -->
			<!-- ============================================================== -->

		</div>
		@include('new_layout.footer')
		<script type="text/javascript">
			function detail(id){
				window.location = "/admin/komisioner/detail/"+id;
			}
			function tambah(){
				window.location = "/admin/komisioner/insert";
			}
		</script>