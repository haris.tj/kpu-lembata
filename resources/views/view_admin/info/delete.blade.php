<button class="btn-rounded btn-danger btn-xs" data-toggle="modal" data-target="#deleteInfox{{$dt->id_info}}">
	<i class="fa fa-trash"></i>
</button>
{{-- <a href="#" >Hapus</a> --}}
<div class="modal fade" id="deleteInfox{{$dt->id_info}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">

				<p class="alert alert-danger">Yakin ingin menghapus Data ini?</p>

			</div>
			<div class="modal-footer">

				<a href="/admin/info/delete/{{$dt->id_info}}" class="btn-rounded btn-sm btn-danger"><i class="ti-trash"></i> Hapus</a>

				<button type="button" class="btn-rounded btn-sm btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
