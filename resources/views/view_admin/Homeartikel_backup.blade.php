@include('admin.header');
@include('admin.sidebar');
<div class="dashboard-wrapper">
	<div class="dashboard-ecommerce">
		<div class="container-fluid dashboard-content ">
			<h4 class="title"> <strong>Data Artikel</strong></h4>
			{{-- Notifikasi DISINI NANTINYA--}}
			@if ($message = Session::get('success'))
			<div class="alert alert-success alert-block">
				<button type="button" class="close" data-dismiss="alert">×</button> 
				<strong>{{ $message }}</strong>
			</div>
			@endif

			@if ($message = Session::get('error'))
			<div class="alert alert-danger alert-block">
				<button type="button" class="close" data-dismiss="alert">×</button> 
				<strong>{{ $message }}</strong>
			</div>
			@endif
			{{-- END OF NOTIFICATION --}}
			<div class="card">
				<div class="header">
					<a href="{{url('/admin/artikel/insert')}}" class="btn btn-info btn-fill btn-sm fas fa-address-card"> Add</a>
				</div>
				<br>
				<div class="content table-responsive table-full-width">
					<table class="table table-hover table-striped" id="datatable">
						<thead>
							<th>No</th>
							<th>Judul</th>
							<th>Artikel</th>
							<th>Tgl Post</th>
							<th>Action</th>
						</thead>
						<tbody>
							<?php $no = 1;?>
							@foreach($artikel as $dt)
							<tr>
								<td>{{ $no++ }}</td>
								<td>{{ str_limit($dt->judul, 20)}}</td>
								<td>{!! str_limit($dt->isi, 40) !!} </td>
								<td>{{ $dt->tanggal }}</td>
								<td>
									
									<a href="/admin/artikel/detail/{{$dt->id}}" class="btn btn-primary btn-fill btn-sm">
										<i class="fa fa-info" aria-hidden="true"></i>
									</a>
									<a href="/admin/artikel/{{$dt->id}}" class="btn btn-warning btn-fill btn-sm">
										{{-- <i class="fa fa-pencil-square-o" aria-hidden="true"></i> --}}
										<i class="fa fa-text-width" style="color: white" aria-hidden="true"></i>
									</a>
									{{-- <a href="/admin/artikel/delete/{{$dt->id}}" class="btn btn-danger btn-fill btn-sm f">
										<i class="fa fa-trash" aria-hidden="true"></i>
									</a> --}}
									@include('view_admin.DeleteArtikel');
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	@include('admin.footer');