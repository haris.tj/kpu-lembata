   @include('new_layout.header');
   @include('new_layout.sidebar');
   <div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
      <div class="container-fluid dashboard-content ">
        <form action="/admin/komisioner/update" method="post" enctype="multipart/form-data" role="form">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            @if ($message = Session::get('error'))
            <div class="alert alert-danger alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button> 
              <strong>{{ $message }}</strong>
            </div>
            @endif
            <div class="card">
              <h5 class="card-header">Update Komisioner</h5>
              <div class="card-body">
                @foreach($data as $dt)
                <input type="hidden" name="id" value="{{$dt->id_komisioner}}">
                <form class="needs-validation" novalidate>
                  <div class="form-group row">
                    <div class="col-md-4">
                      <label for="validationCustom01">Gambar Profile</label>
                      <input type="file" class="form-control" id="gambar" name="gambar" placeholder="gambar" value="{{$dt->gambar}}">
                      <input type="hidden" class="form-control" name="img" value="{{$dt->gambar}}">
                      <div class="valid-feedback">
                        Looks good!
                      </div>
                    </div>
                    <div class="col-md-4">
                      <label for="validationCustom01">Nama</label>
                      <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Lengkap" required value="{{$dt->nama}}">
                      <div class="valid-feedback">
                        Looks good!
                      </div>
                    </div>
                    <div class="col-md-4">
                     <label for="validationCustom01">Tanggal Lahir</label>
                     <input type="date" class="form-control" id="tgl_lahir" name="tgl_lahir" placeholder="Judul Berita" required value="{{$dt->tgl_lahir}}">
                     <div class="valid-feedback">
                      Looks good!
                    </div>
                  </div>

                </div>
                <div class="form-group row">
                 <div class="col-md-6">
                  <label for="validationCustom01">Tempat Lahir</label>
                  <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" placeholder="Tempat Lahir" required value="{{$dt->tempat_lahir}}">
                  <div class="valid-feedback">
                    Looks good!
                  </div>
                </div>
                <div class="col-md-6">
                  <label for="validationCustom01">Jabatan</label>
                  <input type="text" class="form-control" id="jabatan" name="jabatan" placeholder="Jabatan" required value="{{$dt->jabatan}}">
                  <div class="valid-feedback">
                    Looks good!
                  </div>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-6">
                  <label for="validationCustom01">Devisi</label>
                  <input type="text" class="form-control" id="devisi" name="devisi" placeholder="Devisi" required value="{{$dt->devisi}}">
                  <div class="valid-feedback">
                    Looks good!
                  </div>
                </div>
                <div class="col-md-6">
                  <label for="validationCustom01">Korwil</label>
                  <input type="text" class="form-control" id="korwil" name="korwil" placeholder="Korwil" required value="{{$dt->korwil}}">
                  <div class="valid-feedback">
                    Looks good!
                  </div>
                </div>
              </div>
              <div class="row">
               <div class="col-md-6">
                 <label for="validationCustom01">Alamat</label>
                 <input type="text" class="form-control" id="alamat" name="alamat" placeholder="Alamat" required value="{{$dt->alamat}}">
                 <div class="valid-feedback">
                   Looks good!
                 </div>
               </div>
               <div class="col-md-6">
                <label for="validationCustom01">Email</label>
                <input type="text" class="form-control" id="email" name="email" placeholder="mail@mail.com" required value="{{$dt->email}}">
                <div class="valid-feedback">
                  Looks good!
                </div>
              </div>
            </div>
            @endforeach
            <div class="form-group row">
              <button class="btn-rounded btn-sm btn-primary" type="submit">Simpan</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</form>
@include('new_layout.footer');
</div>
</div>