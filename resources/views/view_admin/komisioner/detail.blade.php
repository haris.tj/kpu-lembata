<button class="btn-rounded btn-xs btn-info" data-toggle="modal" data-target="#Detail{{$dt->id_komisioner}}">
	<i class="ti-info"></i>
</button>
<div class="modal fade" id="Detail{{$dt->id_komisioner}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">

				{{-- <p class="alert alert-danger">Yakin ingin menghapus  Album ini ini?<br><span>Data pada album juga akan di HAPUS.!</span></p> --}}
				<div class="row form-group">
					<div class="col-sm-12 text-center">
						<img src="{{url('public/komisioner/'.$dt->gambar)}}" height="380">
					</div>
					{{-- <div class="col-sm-1"></div> --}}
					<div class="col-sm-12" style="color: black">
						<?php
						$q = $dt->tgl_lahir;
						$p = $dt->tempat_lahir;
						$ps = explode("-", $q);
						$t = $ps[0];
						$b = $ps[1];
						$h = $ps[2];
						$nama_bulan = date("F", mktime(0, 0, 0, $b, 10));
						$ttl = $p." , ".$h." - ".$nama_bulan." - ".$t;
						?>
						<hr>
						<p>Nama : {{$dt->nama}}</p>
						{{-- <span>NIP : {{$dt->nip}}</span><br> --}}
						<p>TTL : {{$ttl}}</p>
						<p>Jabatan : {{$dt->jabatan}}</p>
						@if(!empty($dt->devisi))
						<p>Devisi : {{$dt->devisi}}</p>
						@endif
						<p>Korwil : {{$dt->korwil}}</p>
						<p>Alamat : {{$dt->alamat}}</p>
						<p>Email : {{$dt->email}}</p>
					</div>
				</div>


			</div>
			<div class="modal-footer">
				<button type="button" class="btn-rounded btn-sm btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
