<button class="btn-rounded btn-danger btn-xs" data-toggle="modal" data-target="#Deletekm{{$dt->id_komisioner}}">
	<i class="ti-trash"></i>
</button>
<div class="modal fade" id="Deletekm{{$dt->id_komisioner}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">

				<p class="alert alert-danger">Yakin ingin menghapus Data ini?</p>

			</div>
			<div class="modal-footer">

				<a href="/admin/komisioner/delete/{{$dt->id_komisioner}}" class="btn-rounded btn-sm btn-danger"><i class="ti-trash"></i> Hapus</a>

				<button type="button" class="btn-sm btn-rounded btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
