   @include('new_layout.header');
   @include('new_layout.sidebar');
   <div class="dashboard-wrapper">
   	<div class="dashboard-ecommerce">
   		<div class="container-fluid dashboard-content ">
   			<form action="/admin/komisioner/insert/act" method="post" enctype="multipart/form-data" role="form">
   				<input type="hidden" name="_token" value="{{ csrf_token() }}">
   				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
   					@if ($message = Session::get('error'))
   					<div class="alert alert-danger alert-block">
   						<button type="button" class="close" data-dismiss="alert">×</button> 
   						<strong>{{ $message }}</strong>
   					</div>
   					@endif
   					<div class="card">
   						<h5 class="card-header">Tambah Komisioner</h5>
   						<div class="card-body">
   							<form class="needs-validation" novalidate>
   								<div class="form-group row">
   									<div class="col-md-4">
   										<label for="validationCustom01">Gambar Profile</label>
   										<input type="file" class="form-control" id="gambar" name="gambar" placeholder="gambar">
   										<div class="valid-feedback">
   											Looks good!
   										</div>
   									</div>
   									<div class="col-md-4">
   										<label for="validationCustom01">Nama</label>
   										<input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Lengkap" required>
   										<div class="valid-feedback">
   											Looks good!
   										</div>
   									</div>
                    <div class="col-md-4">
                     <label for="validationCustom01">Tanggal Lahir</label>
                     <input type="date" class="form-control" id="tgl_lahir" name="tgl_lahir" placeholder="Judul Berita" required>
                     <div class="valid-feedback">
                      Looks good!
                    </div>
                  </div>

                </div>
                <div class="form-group row">
                 <div class="col-md-6">
                  <label for="validationCustom01">Tempat Lahir</label>
                  <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" placeholder="Tempat Lahir" required>
                  <div class="valid-feedback">
                    Looks good!
                  </div>
                </div>
                <div class="col-md-6">
                  <label for="validationCustom01">Jabatan</label>
                  <input type="text" class="form-control" id="jabatan" name="jabatan" placeholder="Jabatan" required>
                  <div class="valid-feedback">
                    Looks good!
                  </div>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-6">
                  <label for="validationCustom01">Devisi</label>
                  <input type="text" class="form-control" id="devisi" name="devisi" placeholder="Devisi" required>
                  <div class="valid-feedback">
                    Looks good!
                  </div>
                </div>
                <div class="col-md-6">
                  <label for="validationCustom01">Korwil</label>
                  <input type="text" class="form-control" id="korwil" name="korwil" placeholder="Korwil" required>
                  <div class="valid-feedback">
                    Looks good!
                  </div>
                </div>
              </div>
              <div class="row">
               <div class="col-md-6">
                 <label for="validationCustom01">Alamat</label>
                 <input type="text" class="form-control" id="alamat" name="alamat" placeholder="Alamat" required>
                 <div class="valid-feedback">
                   Looks good!
                 </div>
               </div>
               <div class="col-md-6">
                <label for="validationCustom01">Email</label>
                <input type="text" class="form-control" id="email" name="email" placeholder="mail@mail.com" required>
                <div class="valid-feedback">
                  Looks good!
                </div>
              </div>
            </div>
            <div class="form-group row">
              <div class="card-footer">
                <button class="btn-sm btn-rounded btn-primary" type="submit">Simpan</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</form>
@include('new_layout.footer');
</div>
</div>