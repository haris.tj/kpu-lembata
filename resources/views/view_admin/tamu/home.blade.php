@include('new_layout.header')
@include('new_layout.sidebar')
<div class="dashboard-wrapper">
	<div class="container-fluid dashboard-content">
		<!-- ============================================================== -->
		<!-- pageheader -->
		<!-- ============================================================== -->
		<div class="row">
			<!-- ============================================================== -->
			<!-- data table multiselects  -->
			<!-- ============================================================== -->
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">

				<div class="card">
					<div class="card-header">
						<div class="row form-group">
							<div class="col-sm-6">
								{{-- @include('view_admin/info/insert') --}}
							</div>
							<div class="col-sm-6 text-right">
								<h2>Kritik & Saran</h2>
							</div>
						</div>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="example3" class="table table-striped table-bordered" style="width:100%">
								<thead>
									<th>No</th>
									<th>Nama</th>
									<th>Email</th>
									<th>Alamat</th>
									<th>Kritik & Saran</th>
									<th>Tgl</th>
									<th>Action</th>
								</thead>
								<tbody>
									<?php $no = 1;?>
									@foreach($data as $dt)
									<tr>
										<td>{{ $no++ }}</td>
										<td>{{$dt->nama}}</td>
										<td><a href="mailto:{{$dt->email}}">{{$dt->email}}</a></td>
										<td>{{$dt->alamat}}</td>
										<td>{{$dt->pesan}}</td>
										<td>{{$dt->tgl}}</td>
										<td>
											@include('view_admin.tamu.delete')
											{{-- <button class="btn-xs btn-rounded btn-danger" id="hapus" value="{{$dt->id}}">Hapus</button> --}}
										</td>
									</tr>
									@endforeach
								</table>
							</div>
						</div>
					</div>
				</div>
				<!-- ============================================================== -->
				<!-- end data table multiselects  -->
				<!-- ============================================================== -->
			</div>
			<!-- ============================================================== -->
			<!-- end pageheader -->
			<!-- ============================================================== -->

		</div>
		@include('new_layout.footer')
		<script type="text/javascript">
			function detail(id){
				window.location = "/admin/komisioner/detail/"+id;
			}
			function tambah(){
				window.location = "/admin/komisioner/insert";
			}
		</script>