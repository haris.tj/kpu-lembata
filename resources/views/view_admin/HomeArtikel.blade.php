@include('new_layout.header')
@include('new_layout.sidebar')
<!-- ============================================================== -->
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
	<div class="container-fluid dashboard-content">
		<!-- ============================================================== -->
		<!-- pageheader -->
		<!-- ============================================================== -->
		<div class="row">
			<!-- ============================================================== -->
			<!-- data table multiselects  -->
			<!-- ============================================================== -->
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				{{-- Notifikasi DISINI NANTINYA--}}
		
				{{-- END OF NOTIFICATION --}}
				<div class="card">
					<div class="card-header">
						<div class="row form-group">
							<div class="col-sm-6">
								<button class="btn-rounded btn-sm btn-primary" onclick="tambah()"><i class="ti-plus"> Tambah</i></button>
							</div>
							<div class="col-sm-6 text-right">
								<h2>Data Artikel</h2>
							</div>
						</div>
					</div>
					<div class="card-body">
						{{-- START CARD --}}
						<div class="row">
							<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
								{{-- BAGIAN DARI CARI --}}
							{{-- 	<div class="section-block" id="masonary">
									<div class="text-right">
										<input type="text" name="cari" id="cari" placeholder="Cari Dengan Judul">
										<button class="btn-sm btn-primary btn-rounded"><i class="ti-reload"> Cari</i></button>
									</div>
								</div> --}}
								{{-- END OF CARI --}}
							</div>
							<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
								<div class="card-columns">


									@foreach($artikel as $dt)
									<div class="card">
										<div class="card-body">
											<h3 class="card-title">{{ str_limit($dt->judul, 60)}}</h3><hr>
											<p class="card-text">{!! str_limit($dt->isi, 250) !!} </p>

											<?php
											$ps = explode("-",$dt->tanggal);
											$month = $ps[1];
											$dateObj   = DateTime::createFromFormat('!m', $month);
									$monthName = $dateObj->format('F'); // March
									$date = $ps[0]." ".$monthName." ".$ps[2];
									?>

									<p class="card-text">
										<small class="text-muted">{{ $date }}</small>
									</p>
									<div class="card-footer text-right">
										<button class="btn-xs btn-rounded btn-info" id="{{$dt->id}}" onclick="detail(this.id)"><i class="ti-info"></i></button>
										<button class="btn-xs btn-rounded btn-warning" id="{{$dt->id}}" onclick="update(this.id)"><i class="ti-pencil"></i></button>
										@include('view_admin.DeleteArtikel')
									</div>
								</div>
							</div>
							@endforeach
						</div>
					</div>
				</div>
				{{-- END CARD --}}
				<div class="card-footer text-right">
					{{ $artikel->links() }}
				</div>
			</div>
		</div>
		<!-- ============================================================== -->
		<!-- end data table multiselects  -->
		<!-- ============================================================== -->
	</div>
	<!-- ============================================================== -->
	<!-- end pageheader -->
	<!-- ============================================================== -->

</div>
@include('new_layout.footer')
<script type="text/javascript">
	function update(id){
		window.location = "/admin/artikel/"+id;
	}
	function detail(id){
		window.location = "/admin/artikel/detail/"+id;
	}
	function tambah(){
		window.location = "{{url('/admin/artikel/insert')}}";
	}

</script>