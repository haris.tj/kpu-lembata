@include('new_layout.header')
@include('new_layout.sidebar')
<!-- ============================================================== -->
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
	<div class="container-fluid dashboard-content">
		<!-- ============================================================== -->
		<!-- pageheader -->
		<!-- ============================================================== -->
		<div class="row">
			<!-- ============================================================== -->
			<!-- data table multiselects  -->
			<!-- ============================================================== -->
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				{{-- Notifikasi DISINI NANTINYA--}}
			
				{{-- END OF NOTIFICATION --}}
				<div class="card">
					<div class="card-header">
						<div class="row form-group">
							<div class="col-sm-6">
								{{-- <button class="btn-sm btn-rounded btn-primary"><i class="ti-plus"></i> Tambah</button> --}}
								@include('view_admin.galery.insert')
							</div>
							<div class="col-sm-6 text-right">
								<h2>Data Album</h2>
							</div>
						</div>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="example3" class="table table-striped table-bordered" style="width:100%">
								<thead>
									<tr>
										<th>No</th>
										<th>Album Name</th>
										<th>Tanggal</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php $no = 1;?>
									@foreach($data as $dt)
									<tr>
										<td>{{ $no++ }}</td>
										<td>{{$dt->nama_album}}</td>
										<td>{{$dt->waktu}}</td>
										<td>
										{{-- 	<a href="/admin/album/detail/{{$dt->id_album}}" class="btn-xs btn-info btn-rounded">
												<i class="ti-info"></i>
											</a> --}}
											<button onclick="btn_info(this.id)" id="{{$dt->id_album}}"class="btn-rounded btn-info btn-xs">
												<i class="ti-info"></i>
											</button>
											@include('view_admin.galery.update')
											{{-- LOAD MODAL FROM VIEWADMIN.DELETE --}}
											@include('view_admin.galery.insert_IMG')
											@include('view_admin.galery.delete')
										</td>
									</tr>
									@endforeach
								</table>
							</div>
						</div>
					</div>
				</div>
				<!-- ============================================================== -->
				<!-- end data table multiselects  -->
				<!-- ============================================================== -->
			</div>
			<!-- ============================================================== -->
			<!-- end pageheader -->
			<!-- ============================================================== -->

		</div>
		@include('new_layout.footer')
		{{-- @include('sweetalert::alert') --}}
		<script type="text/javascript">
			function btn_info(id) {
		// var id = document.getElementById('btn-info').value;
		window.location = "/admin/album/detail/"+id;
	}
</script>