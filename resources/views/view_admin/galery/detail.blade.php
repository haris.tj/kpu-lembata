@include('new_layout.header')
@include('new_layout.sidebar')
<div class="dashboard-wrapper">
	<div class="dashboard-ecommerce">
		<div class="container-fluid dashboard-content ">
			{{--  --}}


			{{--  --}}
			@foreach($data as $dt)
			<?php
			$nama = $dt->nama_gambar;
			$exp = explode('|', $nama);
			?>
		{{-- 	<h3>Gambar Dari Album : {{$dt->nama_album}}</h3>
		<hr> --}}
		<div class="row form-group">
			<div class="col-sm-1"></div>
			<div class="col-xl-10 col-lg-12 col-md-12 col-sm-12 col-12">
				<div class="card">
					<h5 class="card-header">Gambar Dari Album : {{$dt->nama_album}}</h5>
					<div class="card-body">
						<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
							<ol class="carousel-indicators">
								<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
								<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
								<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
							</ol>
							<div class="carousel-inner">
								<div class="carousel-item active">
									<img class="d-block w-100" src="http://placehold.it/1200x600/555/000&text={{$dt->nama_album}}" alt="First slide">
								</div>
								@for($x=0; $x<count($exp); $x++)
								<div class="carousel-item">
									<img class="d-block w-100" src="{{url('image/'.$exp[$x])}}" alt="Second slide">
								</div>
								@endfor
							</div>
							<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
								<span class="carousel-control-prev-icon" aria-hidden="true"></span>
								<span class="sr-only">Previous</span>  </a>
								<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
									<span class="carousel-control-next-icon" aria-hidden="true"></span>
									<span class="sr-only">Next</span>  </a>
								</div>
							</div>
						</div>
					</div>
				</div>
				@endforeach
				{{--  --}}
				{{--  --}}
			</div>
		</div>

		@include('new_layout.footer')
