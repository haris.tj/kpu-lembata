<button class="btn-rounded btn-sm btn-primary" data-toggle="modal" data-target="#insertAlbum"><i class="ti-plus"></i> Tambah</button>
<div class="modal fade" id="insertAlbum" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4>Tambah Album</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times</button>
			</div>
			<form action="/admin/album/insert" method="post" enctype="multipart/form-data" role="form">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				{{method_field('patch')}}
				<div class="modal-body">
					<div class="row">
						{{-- <input type="hidden" name="id" value="{{$dt->id_agenda}}"> --}}
						<div class="col-md-12">
							<label for="validationCustom01">Album Name</label>
							<input type="text" class="form-control" id="nama" name="nama">
							<div class="valid-feedback">
								Looks good!
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn-rounded btn-sm btn-info">Save</button>
					<button type="button" class="btn-rounded btn-sm btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>
