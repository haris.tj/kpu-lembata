<button class="btn-xs btn-rounded btn-danger" data-toggle="modal" data-target="#Delete{{$dt->id_album}}">
	<i class="ti-trash"></i>
</button>
<div class="modal fade" id="Delete{{$dt->id_album}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">

				<p class="alert alert-danger">Yakin ingin menghapus  Album ini ini?<br><span>Data pada album juga akan di HAPUS.!</span></p>


			</div>
			<div class="modal-footer">

				<a href="/admin/album/delete/{{$dt->id_album}}" class="btn-rounded btn-danger btn-sm"><i class="ti-trash"></i>Hapus</a>

				<button type="button" class="btn-rounded btn-default btn-sm" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
