
<button class="btn-xs btn-rounded btn-warning" data-toggle="modal" data-target="#updateAlbum{{$dt->id_album}}" id="{{$dt->id_album}}" onclick="reply_click()">
	<i class="ti-pencil"></i>
</button>
<div class="modal fade" id="updateAlbum{{$dt->id_album}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4>Edit Album</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times</button>
			</div>
			<form action="/admin/album/update" method="post" enctype="multipart/form-data" role="form">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="id" value="{{$dt->id_album}}" id="id">
				{{method_field('patch')}}
				<div class="modal-body">
					<div class="row">
						{{-- <input type="hidden" name="id" value="{{$dt->id_agenda}}"> --}}
						<div class="col-md-12">
							<label for="validationCustom01">Album Name</label>
							<input type="text" class="form-control" id="nama" name="nama" value="{{$dt->nama_album}}">
							<div class="valid-feedback">
								Looks good!
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn-rounded btn-info btn-sm">Save</button>
					<button type="button" class="btn-rounded btn-default btn-sm" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	function reply_click()
	{	var id = event.srcElement.id;
		var x = document.getElementById('id').value = id;
	}
</script>