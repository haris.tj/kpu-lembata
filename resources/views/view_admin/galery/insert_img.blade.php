<button class="btn-xs btn-primary btn-rounded" data-toggle="modal" data-target="#insertGambar{{$dt->id_album}}" value="{{$dt->id_album}}" id="{{$dt->id_album}}" name="click" onclick="reply_click()"><i class="ti-plus"></i></button>
<div class="modal fade" id="insertGambar{{$dt->id_album}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4>Tambah Gambar</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times</button>
			</div>
			<form action="/admin/galery/insert" method="post" enctype="multipart/form-data" role="form">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				{{method_field('patch')}}
				<div class="modal-body">
					<div class="row">
						<input type="hidden" name="id" value="{{$dt->id_album}}" id="id">
						<div class="col-md-12">
							<label for="validationCustom01">Image</label>
							<input type="file" class="form-control" name="images[]" placeholder="file_img" multiple>
							<label class="text-danger">Multiple Upload IMG</label>
							<div class="valid-feedback">
								Looks good!
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn-rounded btn-info btn-sm">Save</button>
					<button type="button" class="btn-rounded btn-default btn-sm" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	function reply_click()
	{	var id = event.srcElement.id;
		var x = document.getElementById('id').value = id;
	}
</script>