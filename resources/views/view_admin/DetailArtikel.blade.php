 @include('new_layout.header')
 @include('new_layout.sidebar')
 <div class="dashboard-wrapper">
 	<div class="dashboard-ecommerce">
 		<div class="container-fluid dashboard-content ">
 			<!-- ============================================================== -->
 			<!-- pageheader  -->
 			<!-- ============================================================== -->
 			@foreach($detail as $dt)
 			<div class="card">
 				<h5 class="card-header">{{$dt->judul}}</h5>
 				<div class="container-fluid">
 					<?php
 					$ps = explode("-",$dt->tanggal);
 					$month = $ps[1];
 					$dateObj   = DateTime::createFromFormat('!m', $month);
									$monthName = $dateObj->format('F'); // March
									$date = $ps[0]." ".$monthName." ".$ps[2];
									?>
									<p class="card-text">
										<small class="text-muted">Tgl Post : {{ $date }}</small><br>
										<small>Visit : {{$dt->visits}}</small>
									</p>
								</div>
								<div class="card-body">
									<div class="row form-group">
										<div class="col-lg-1"></div>
										<div class="col-lg-10">
											<p>{!! $dt->isi !!}</p>
										</div>
									</div>
								</div>
								@endforeach
							</div>
							{{-- <a href="/admin/artikel" class="btn btn-sm btn-info">Kembali</a> --}}
							<button class="btn-rounded btn-sm btn-primary" onclick="kembali()">Kembali</button>
						</div>
					</div>
					<!-- ============================================================== -->
					<!-- END OF WRAPPER KONTEN -->
					<!-- ============================================================== -->    
					<!-- ============================================================== -->
					<!-- footer -->
					<!-- ============================================================== -->
					@include('new_layout.footer')
					<script type="text/javascript">
						function kembali() {
							window.location = "/admin/artikel";
						}
					</script>