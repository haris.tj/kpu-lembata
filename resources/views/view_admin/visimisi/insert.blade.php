   @include('new_layout.header');
   @include('new_layout.sidebar');
   <div class="dashboard-wrapper">
   	<div class="dashboard-ecommerce">
   		<div class="container-fluid dashboard-content ">
   			<form action="/admin/visi-misi/insert/act" method="post" enctype="multipart/form-data" role="form">
   				<input type="hidden" name="_token" value="{{ csrf_token() }}">
   				<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.0.15/tinymce.min.js" referrerpolicy="origin"></script>
   				<script type="text/javascript">
   					tinymce.init({selector:'textarea'});
   				</script>
   				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
             @if ($message = Session::get('error'))
             <div class="alert alert-danger alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button> 
              <strong>{{ $message }}</strong>
            </div>
            @endif
            <div class="card">
              <h5 class="card-header">Tambah Visi - Misi</h5>
              <div class="card-body">
                <form class="needs-validation" novalidate>
                 <div class="row">

                  <div class="col-md-12">
                    <div class="form-group">
                      <h4>Visi</h4>
                      <textarea name="visi" id="visi" class="form-control" rows="10"></textarea>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <h4>Misi</h4>
                      <textarea name="misi" id="misi" class="form-control" rows="10"></textarea>
                    </div>
                  </div>

                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 ">
                 <button class="btn-sm btn-rounded btn-primary" type="submit">Simpan</button>
               </div>
             </div>
           </form>
         </div>
       </div>
     </div>
   </form>
   @include('new_layout.footer');
 </div>
</div>