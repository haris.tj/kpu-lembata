@include('new_layout.header')
@include('new_layout.sidebar')
<!-- ============================================================== -->
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
	<div class="container-fluid dashboard-content">
		<!-- ============================================================== -->
		<!-- pageheader -->
		<!-- ============================================================== -->
		<div class="row">
			<!-- ============================================================== -->
			<!-- data table multiselects  -->
			<!-- ============================================================== -->
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				{{-- Notifikasi DISINI NANTINYA--}}
			
				{{-- END OF NOTIFICATION --}}
				<div class="card">
					<div class="card-header">
						<div class="form-group row">
							<div class="col-sm-6">
								<h5 class="mb-0">Visi - Misi</h5>
							</div>
							<div class="col-sm-6">
								<div class="text-right">
									{{-- <a href="{{url('admin/visi-misi/insert')}}" class="btn-sm btn-rounded btn-success">Tambah</a> --}}
									@foreach($data as $dt)
									{{-- <a href="/admin/visi-misi/detail/{{$dt->id}}" class="btn-sm btn-rounded btn-warning">Ubah</a> --}}
									<button class="btn-sm btn-rounded btn-primary" onclick="tambah()">Tambah</button>
									<button class="btn-sm btn-rounded btn-warning" onclick="ubah(this.id)" id="{{$dt->id}}">Ubah</button>
									@include('view_admin.visimisi.delete')
								</div>
							</div>
						</div>
					</div>

					<div class="card-body">
						<h3>VISI</h3><hr>
						{!! $dt->visi !!}
						<hr>
						<h3>MISI</h3><hr>
						{!! $dt->misi !!}
					</div>
					@endforeach
				</div>
				<!-- ============================================================== -->
				<!-- end data table multiselects  -->
				<!-- ============================================================== -->
			</div>
			<!-- ============================================================== -->
			<!-- end pageheader -->
			<!-- ============================================================== -->

		</div>
		@include('new_layout.footer')
		<script type="text/javascript">
			function tambah() {
				window.location = "{{url('admin/visi-misi/insert')}}";
			}
			function ubah(id){
				window.location = "/admin/visi-misi/detail/"+id;
			}
		</script>