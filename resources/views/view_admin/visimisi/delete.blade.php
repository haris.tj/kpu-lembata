<button class="btn-sm btn-rounded btn-danger" data-toggle="modal" data-target="#Deletevm{{$dt->id}}">
	Hapus
</button>
{{-- <a href="#" >Hapus</a> --}}
<div class="modal fade" id="Deletevm{{$dt->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">

				<p class="alert alert-danger">Yakin ingin menghapus Data ini?</p>

			</div>
			<div class="modal-footer">

				<a href="/admin/visi-misi/delete/{{$dt->id}}" class="btn-sm btn-rounded btn-danger"><i class="ti-trash"></i> Hapus</a>

				<button type="button" class="btn-sm btn-rounded btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
