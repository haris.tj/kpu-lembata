{{-- <a href="#" class="btn btn-warning btn-fill btn-sm" data-toggle="modal" data-target="#updateHukum{{$dt->id_menu}}">
	<i class="fa fa-text-width" style="color: white" aria-hidden="true"></i>
</a>  --}}
<button class="btn-xs btn-rounded btn-warning" data-toggle="modal" data-target="#updateHukum{{$dt->id_menu}}">
	<i class="ti-pencil"></i>
</button>
<div class="modal fade" id="updateHukum{{$dt->id_menu}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5>Update Produk Hukum</h5>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times</button>
			</div>
			<form action="/admin/hukum/update" method="post" enctype="multipart/form-data" role="form">
				{{-- @foreach($data as $dt) --}}
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="id" value="{{$dt->id_menu}}" id="id">
				{{method_field('patch')}}
				<div class="modal-body">
					<div class="row form-group">
						{{-- <input type="hidden" name="id" value="{{$dt->id_agenda}}"> --}}
						<div class="col-md-12">

							<label for="validationCustom01">Nama Menu</label>
							<select class="form-control" name="nama">
								<?php
								$countries = array('Data pemilu', 'Formulir C1', 'Hasil Pilkada','Undang-undang','PKPU 2015','PKPU 2016','PKPU 2017','PKPU 2018','PKPU 2019','PKPU 2020','PKPU 2021','PKPU 2022','PKPU 2023','PKPU 2024','PKPU 2025','Lain-lain');
								$current_country = $dt->nama_menu;

								foreach($countries as $country) {
									if($country == $current_country) {
										echo '<option selected="selected">'.$country.'</option>';
									} else {
										echo '<option>'.$country.'</option>';
									}
								}
								?>
							</select>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-sm-12">
							<label class="validationCustom01">Judul</label>
							<input type="text" name="judul" class="form-control" value="{{$dt->judul}}">
						</div>
					</div>
					<div class="row form-group">
						<div class="col-sm-12">
							<label class="validationCustom01">Keterangan</label>
							<textarea class="form-control" name="keterangan">{{$dt->keterangan}}</textarea>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-sm-12">
							<label class="validationCustom01">Berkas</label>
							<input type="file" name="file" class="form-control">
							<input type="hidden" name="file2" value="{{$dt->file}}">
						</div>
					</div>
				</div>
				{{-- @endforeach --}}
				<div class="modal-footer">
					<button type="submit" class="btn-sm btn-rounded btn-info">Save</button>
					<button type="button" class="btn-sm btn-rounded btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>
{{-- <script type="text/javascript">
	function reply_click()
	{	var id = event.srcElement.id;
		var x = document.getElementById('id').value = id;
	}
</script> --}}