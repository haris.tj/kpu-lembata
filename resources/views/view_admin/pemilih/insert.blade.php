{{-- <a href="#" class="btn-xs btn-info btn-fill" data-toggle="modal" data-target="#insertpemilih"> Add</a>  --}}
<button class="btn-rounded btn-sm btn-primary" data-toggle="modal" data-target="#insertpemilih">
	<i class="ti-plus"> Tambah</i>
</button>
<div class="modal fade" id="insertpemilih" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5>Insert Pemilih</h5>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times</button>
			</div>
			<form action="/admin/pemilih/insert/act" method="post" enctype="multipart/form-data" role="form">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="id" value="" id="id">
				{{method_field('patch')}}
				<div class="modal-body">
					<div class="row form-group">
						{{-- <input type="hidden" name="id" value="{{$dt->id_agenda}}"> --}}
						<div class="col-md-12">
							<label for="validationCustom01">Nama Menu</label>
							<select class="form-control" name="nama">
								<option value="DATA PEMILIH PILEG">DATA PEMILIH PILEG</option>
								<option value="DATA PEMILIH PILPRES">DATA PEMILIH PILPRES</option>
								<option value="DATA PEMILIH PILKADA">DATA PEMILIH PILKADA</option>
							</select>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-sm-12">
							<label class="validationCustom01">Judul</label>
							<input type="text" name="judul" class="form-control">
						</div>
					</div>
					<div class="row form-group">
						<div class="col-sm-12">
							<label class="validationCustom01">Keterangan</label>
							<textarea class="form-control" name="keterangan"></textarea>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-sm-12">
							<label class="validationCustom01">Berkas</label>
							<input type="file" name="file" class="form-control">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn-rounded btn-sm btn-info">Save</button>
					<button type="button" class="btn-rounded btn-sm btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>
{{-- <script type="text/javascript">
	function reply_click()
	{	var id = event.srcElement.id;
		var x = document.getElementById('id').value = id;
	}
</script> --}}