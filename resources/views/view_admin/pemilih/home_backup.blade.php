@include('admin.header');
@include('admin.sidebar');
<div class="dashboard-wrapper">
	<div class="dashboard-ecommerce">
		<div class="container-fluid dashboard-content ">
			<h4 class="title"> <strong>Produk Hukum</strong></h4>
			{{-- Notifikasi DISINI NANTINYA--}}
			@if ($message = Session::get('success'))
			<div class="alert alert-success alert-block">
				<button type="button" class="close" data-dismiss="alert">×</button> 
				<strong>{{ $message }}</strong>
			</div>
			@endif

			@if ($message = Session::get('error'))
			<div class="alert alert-danger alert-block">
				<button type="button" class="close" data-dismiss="alert">×</button> 
				<strong>{{ $message }}</strong>
			</div>
			@endif
			{{-- END OF NOTIFICATION --}}
			<div class="card">
				<div class="header">
					{{-- <a href="{{url('/admin/hukum/insert')}}" class="btn btn-info btn-fill btn-sm fas fa-address-card"> Add</a> --}}
					@include('view_admin/hukum/insert')
				</div>
				<br>
				<div class="content table-responsive table-full-width">
					<table class="table table-hover table-striped" id="datatable">
						<thead>
							<th>No</th>
							<th>Nama Menu</th>
							<th>Judul</th>
							<th>Keterangan</th>
							<th>Action</th>
						</thead>
						<tbody>
							<?php $no = 1;?>
							@foreach($data as $dt)
							<tr>
								<td>{{ $no++ }}</td>
								<td>{{$dt->nama_menu}}</td>
								<td>{{$dt->judul}} </td>
								<td>{{$dt->keterangan}}</td>
								<td>
									@include('view_admin.hukum.update')
									@include('view_admin.hukum.delete')
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	@include('admin.footer');