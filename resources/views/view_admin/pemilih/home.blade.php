@include('new_layout.header')
@include('new_layout.sidebar')
<div class="dashboard-wrapper">
	<div class="container-fluid dashboard-content">
		<!-- ============================================================== -->
		<!-- pageheader -->
		<!-- ============================================================== -->
		<div class="row">
			<!-- ============================================================== -->
			<!-- data table multiselects  -->
			<!-- ============================================================== -->
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				{{-- Notifikasi DISINI NANTINYA--}}
		
				{{-- END OF NOTIFICATION --}}
				<div class="card">
					<div class="card-header">
						<div class="row form-group">
							<div class="col-sm-6">
								{{-- <button class="btn-sm btn-rounded btn-primary" onclick="tambah()"><i class="ti-plus"></i> Tambah</button> --}}
								{{-- <a href="/admin/agenda/insert" class="btn-lg btn-rounded btn-primary"><i class="ti-plus"></i> Tambah</a> --}}
								@include('view_admin/pemilih/insert')
							</div>
							<div class="col-sm-6 text-right">
								<h2>Data Pemilih</h2>
							</div>
						</div>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="example3" class="table table-striped table-bordered" style="width:100%">
								<thead>
									<tr>
										<th>No</th>
										<th>Nama Menu</th>
										<th>Judul</th>
										<th>Keterangan</th>
										<th>File</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php $no = 1;?>
									@foreach($data as $dt)
									<tr>
										<td>{{ $no++ }}</td>
										<td>{{$dt->nama_menu}}</td>
										<td>{{$dt->judul}} </td>
										<td>{{$dt->keterangan}}</td>
										<td><a href="../public/uploads/data-pemilih/{{$dt->file}}" target="_blank">{{$dt->file}}</a></td>
										<td>
											@include('view_admin.pemilih.update')
											@include('view_admin.pemilih.delete')
										</td>
									</tr>
									@endforeach
								</table>
							</div>
						</div>
					</div>
				</div>
				<!-- ============================================================== -->
				<!-- end data table multiselects  -->
				<!-- ============================================================== -->
			</div>
			<!-- ============================================================== -->
			<!-- end pageheader -->
			<!-- ============================================================== -->

		</div>
		@include('new_layout.footer')
		<script type="text/javascript">
			function detail(id){
				window.location = "/admin/komisioner/detail/"+id;
			}
			function tambah(){
				window.location = "/admin/komisioner/insert";
			}
		</script>