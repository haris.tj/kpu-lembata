@include('admin.header');
@include('admin.sidebar');
<div class="dashboard-wrapper">
	<div class="dashboard-ecommerce">
		<div class="container-fluid dashboard-content ">
			<h4 class="title"> <strong>Data Profile</strong></h4>
			{{-- Notifikasi DISINI NANTINYA--}}
			@if ($message = Session::get('success'))
			<div class="alert alert-success alert-block">
				<button type="button" class="close" data-dismiss="alert">×</button> 
				<strong>{{ $message }}</strong>
			</div>
			@endif

			@if ($message = Session::get('error'))
			<div class="alert alert-danger alert-block">
				<button type="button" class="close" data-dismiss="alert">×</button> 
				<strong>{{ $message }}</strong>
			</div>
			@endif
			{{-- END OF NOTIFICATION --}}
			<div class="card">
				<div class="header">
					<a href="/admin/user/insert" class="btn btn-info btn-fill btn-sm fas fa-address-card"> Add</a>
				</div>
				<br>
				<div class="content table-responsive table-full-width">
					<table class="table table-hover table-striped" id="datatable">
						<thead>
							<th>No</th>
							<th>NIP</th>
							<th>Nama</th>
							<th>Jabatan</th>
							<th>Action</th>
						</thead>
						<tbody>
							<?php $no = 1;?>
							@foreach($data as $dt)
							<tr>
								<td>{{ $no++ }}</td>
								<td>{{$dt->nip}}</td>
								<td>{{$dt->nama}} </td>
								<td>{{$dt->jabatan}}</td>
								<td>
									@include('view_admin.profilsek.detail')
									{{-- <button class="btn btn-round btn-sm btn-primary">test</button> --}}
									<a href="/admin/user/detail/{{$dt->id_psek}}" class="btn btn-warning btn-fill btn-sm">
										<i class="fa fa-text-width" style="color: white" aria-hidden="true"></i>
									</a>
									@include('view_admin.profilsek.delete')
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	@include('admin.footer');