   @include('new_layout.header');
   @include('new_layout.sidebar');
   <div class="dashboard-wrapper">
   	<div class="dashboard-ecommerce">
   		<div class="container-fluid dashboard-content ">
   			<form action="/admin/user/update" method="post" enctype="multipart/form-data" role="form">
   				<input type="hidden" name="_token" value="{{ csrf_token() }}">
   				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
   					@if ($message = Session::get('error'))
   					<div class="alert alert-danger alert-block">
   						<button type="button" class="close" data-dismiss="alert">×</button> 
   						<strong>{{ $message }}</strong>
   					</div>
   					@endif
   					<div class="card">
   						<h5 class="card-header">Update User</h5>
   						<div class="card-body">
                @foreach($data as $dt)
                <input type="hidden" name="id" value="{{$dt->id_psek}}">
   							<form class="needs-validation" novalidate>
   								<div class="form-group row">
   									<div class="col-md-4">
   										<label for="validationCustom01">Gambar Profile</label>
   										<input type="file" class="form-control" id="gambar" name="gambar" placeholder="gambar" value="{{$dt->gambar}}">
                      <input type="hidden" class="form-control" name="img" value="{{$dt->gambar}}">
   										<div class="valid-feedback">
   											Looks good!
   										</div>
   									</div>
   									<div class="col-md-4">
   										<label for="validationCustom01">Nama</label>
   										<input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Lengkap" required value="{{$dt->nama}}">
   										<div class="valid-feedback">
   											Looks good!
   										</div>
   									</div>
   									<div class="col-md-4">
   										<label for="validationCustom01">NIP</label>
   										<input type="number" class="form-control" id="nip" name="nip" placeholder="NIP" required value="{{$dt->nip}}">
   										<div class="valid-feedback">
   											Looks good!
   										</div>
   									</div>
   								</div>
   								<div class="form-group row">
   									<div class="col-md-4">
   										<label for="validationCustom01">Tempat Lahir</label>
   										<input type="text" class="form-control" id="tempat" name="tempat" placeholder="Tempat Lahir" required value="{{$dt->tempat_lahir}}">
   										<div class="valid-feedback">
   											Looks good!
   										</div>
   									</div>
   									<div class="col-md-4">
   										<label for="validationCustom01">Tanggal Lahir</label>
   										<input type="date" class="form-control" id="tgl" name="tgl" placeholder="Judul Berita" value="{{$dt->ttl}}">
   										<div class="valid-feedback">
   											Looks good!
   										</div>
   									</div>
   									<div class="col-md-4">
   										<label for="validationCustom01">Jabatan</label>
                       <select class="form-control" name="jabatan">
                        <option value="Sekertaris">Sekertaris</option>
                        <option value="Sub Bagian Hukum">Sub Bagian Hukum</option>
                        <option value="Sub Bagian Teknik Dan Hupmas">Sub Bagian Teknik Dan Hupmas</option>
                        <option value="Sub Bagian Program Dan Data">Sub Bagian Program Dan Data</option>
                        <option value="Sub Bagian Umum">Sub Bagian Umum</option>
                      </select>
                      {{-- <input type="text" class="form-control" id="jabatan" name="jabatan" placeholder="jabatan" required> --}}
                      <div class="valid-feedback">
                        Looks good!
                      </div>
                    </div>
                  </div>
                  <div class="row">
                   <div class="col-md-12">
                     <label for="validationCustom01">Alamat</label>
                     <input type="text" class="form-control" id="alamat" name="alamat" placeholder="Alamat" required value="{{$dt->alamat}}">
                     <div class="valid-feedback">
                       Looks good!
                     </div>
                   </div>
                 </div>
                 <div class="form-group row">
                  <div class="col-md-6">
                    <label for="validationCustom01">Email</label>
                    <input type="text" class="form-control" id="email" name="email" placeholder="mail@mail.com" required value="{{$dt->email}}">
                    <div class="valid-feedback">
                      Looks good!
                    </div>
                  </div>
                  <div class="col-md-6">
                   <label for="validationCustom01">Telepon</label>
                   <input type="text" class="form-control" id="telepeon" name="telepon" placeholder="08122570400000" required value="{{$dt->telepon}}">
                   <div class="valid-feedback">
                     Looks good!
                   </div>
                 </div>
               </div>
               @endforeach
               <div class="form-group row">
                <div class="card-footer">
                  <button class="btn-sm  btn-rounded btn-primary" type="submit">Simpan</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </form>
  @include('new_layout.footer');
</div>
</div>