@include('new_layout.header')
@include('new_layout.sidebar')
<!-- ============================================================== -->
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
	<div class="container-fluid dashboard-content">
		<!-- ============================================================== -->
		<!-- pageheader -->
		<!-- ============================================================== -->
		<div class="row">
			<div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12">
				<div class="card">
					<div class="card-body">
						<div class="d-inline-block">
							<h5 class="text-muted">Total Berita</h5>
							<h2 class="mb-0 text-center">{{$berita}}</h2>
						</div>
						<div class="float-right icon-circle-medium  icon-box-lg  bg-info-light mt-1">
							<i class="fa fa-eye fa-fw fa-sm text-info"></i>
						</div>
					</div>
				</div>
			</div>
			<!-- ============================================================== -->
			<!-- end total views   -->
			<!-- ============================================================== -->
			<!-- ============================================================== -->
			<!-- total followers   -->
			<!-- ============================================================== -->
			<div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12">
				<div class="card">
					<div class="card-body">
						<div class="d-inline-block">
							<h5 class="text-muted">Total Artikel</h5>
							<h2 class="mb-0 text-center">{{$artikel}}</h2>
						</div>
						<div class="float-right icon-circle-medium  icon-box-lg  bg-primary-light mt-1">
							<i class="fa fa-user fa-fw fa-sm text-primary"></i>
						</div>
					</div>
				</div>
			</div>
			<!-- ============================================================== -->
			<!-- end total followers   -->
			<!-- ============================================================== -->
			<!-- ============================================================== -->
			<!-- partnerships   -->
			<!-- ============================================================== -->
			<div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12">
				<div class="card">
					<div class="card-body">
						<div class="d-inline-block">
							<h5 class="text-muted">Total Album</h5>
							<h2 class="mb-0 text-center">{{$album}}</h2>
						</div>
						<div class="float-right icon-circle-medium  icon-box-lg  bg-secondary-light mt-1">
							<i class="fa fa-handshake fa-fw fa-sm text-secondary"></i>
						</div>
					</div>
				</div>
			</div>
			<!-- ============================================================== -->
			<!-- end partnerships   -->
			<!-- ============================================================== -->
			<!-- ============================================================== -->
			<!-- total earned   -->
			<!-- ============================================================== -->
			<div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12">
				<div class="card">
					<div class="card-body">
						<div class="d-inline-block">
							<h5 class="text-muted">Total Agenda</h5>
							<h2 class="mb-0 text-center">{{$agenda}}</h2>
						</div>
						<div class="float-right icon-circle-medium  icon-box-lg  bg-brand-light mt-1">
							<i class="fa fa-money-bill-alt fa-fw fa-sm text-brand"></i>
						</div>
					</div>
				</div>
			</div>
		</div>

		{{--  --}}
		<!-- ============================================================== -->
		<div class="row form-group">
			<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
				<div class="section-block">
					<h5 class="section-title">Berita Terbaru</h5>
					{{-- <p>Takes the basic nav from above and adds the .nav-tabs class to generate a tabbed interface..</p> --}}
				</div>
				<div class="accrodion-regular">
					<div id="accordion3">
						{{--  --}}
						@foreach($berita3 as $br)
						<div class="card mb-2">
							<div class="card-header" id="headingEight">
								<h5 class="mb-0">
									<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#databerita{{$br->id}}" aria-expanded="false" aria-controls="databerita{{$br->id}}">
										<span class="fas fa-angle-down mr-3"></span>{{str_limit($br->subject, 50)}}
									</button>       </h5>
								</div>
								<div id="databerita{{$br->id}}" class="collapse" aria-labelledby="headingEight" data-parent="#accordion3">
									<div class="card-body">
										{!! str_limit($br->isi, 200) !!}
									</div>
									<a href="/admin/berita/detail/{{$br->id}}" class="form-control">Selengkapnya</a>
								</div>
							</div>
							@endforeach
						</div>
					</div>
				</div>
				{{--  --}}
				<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
					<div class="section-block">
						<h5 class="section-title">Artikel Terbaru</h5>
						{{-- <p>Takes the basic nav from above and adds the .nav-tabs class to generate a tabbed interface..</p> --}}
					</div>
					<div class="accrodion-regular">
						<div id="accordion3">
							{{--  --}}
							@foreach($artikel3 as $ar)
							<div class="card mb-2">
								<div class="card-header" id="headingEight">
									<h5 class="mb-0">
										<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#dataartikel{{$ar->id}}" aria-expanded="false" aria-controls="dataartikel{{$ar->id}}">
											<span class="fas fa-angle-down mr-3"></span>{{str_limit($ar->judul,50)}}
										</button>       </h5>
									</div>
									<div id="dataartikel{{$ar->id}}" class="collapse" aria-labelledby="headingEight" data-parent="#accordion3">
										<div class="card-body">
											{!! str_limit($ar->isi, 200) !!}
										</div>
										<a href="/admin/artikel/detail/{{$ar->id}}" class="form-control">Selengkapnya</a>
									</div>
								</div>
								@endforeach
							</div>
						</div>
					</div>
				</div>
				<!-- ============================================================== -->
				@include('new_layout.footer')