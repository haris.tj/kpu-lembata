<button class="btn-xs btn-rounded btn-warning" data-toggle="modal" data-target="#Status{{$dt->id_agenda}}">
	<i class="ti-pencil"></i>
</button>

<div class="modal fade" id="Status{{$dt->id_agenda}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4>Update Agenda</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times</button>
			</div>
			<form action="/agenda/update" method="post" enctype="multipart/form-data" role="form">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				{{method_field('patch')}}
				<div class="modal-body">
					<div class="row">
						<input type="hidden" name="id" value="{{$dt->id_agenda}}">
						<div class="col-md-6">
							<?php
							$tgl = $dt->tanggal;
							$bln = $dt->bulan;
							if ($bln < 10) {
								$bln = "0".$bln;
							}
							$thn = $dt->tahun;
							$date = $thn."-".$bln."-".$tgl;
							?>
							<label for="validationCustom01">Waktu</label>
							<input type="date" class="form-control" id="date" name="date" required value="{{$date}}" required>
							<div class="valid-feedback">
								Looks good!
							</div>
						</div>
						<div class="col-md-6">
							<label for="validationCustom01">Acara</label>
							<input type="text" class="form-control" id="acara" name="acara" value="{{$dt->acara}}" required>
							<div class="valid-feedback">
								Looks good!
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Kegiatan</label>
								<input type="text" class="form-control" id="kegiatan" name="kegiatan" value="{{$dt->kegiatan}}" required>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn-sm btn-rounded btn-info">Save</button>
					<button type="button" class="btn-sm btn-rounded btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>
