@include('admin.header');
@include('admin.sidebar');
<div class="dashboard-wrapper">
	<div class="dashboard-ecommerce">
		<div class="container-fluid dashboard-content ">
			<h4 class="title"> <strong>Data Agenda</strong></h4>
			{{-- Notifikasi DISINI NANTINYA--}}
			@if ($message = Session::get('success'))
			<div class="alert alert-success alert-block">
				<button type="button" class="close" data-dismiss="alert">×</button> 
				<strong>{{ $message }}</strong>
			</div>
			@endif

			@if ($message = Session::get('error'))
			<div class="alert alert-danger alert-block">
				<button type="button" class="close" data-dismiss="alert">×</button> 
				<strong>{{ $message }}</strong>
			</div>
			@endif
			{{-- END OF NOTIFICATION --}}
			<div class="card">
				<div class="header">
					<a href="/admin/agenda/insert" class="btn btn-info btn-fill btn-sm fas fa-address-card"> Add</a>
				</div>
				<br>
				<div class="content table-responsive table-full-width">
					<table class="table table-hover table-striped" id="datatable">
						<thead>
							<th>No</th>
							<th>Waktu</th>
							<th>Acara</th>
							<th>Kegiatan</th>
							<th>Action</th>
						</thead>
						<tbody>
							<?php $no = 1;?>
							@foreach($agenda as $dt)
							<tr>
								<td>{{ $no++ }}</td>
								<?php
								$monthNum  = $dt->bulan;
								$dateObj   = DateTime::createFromFormat('!m', $monthNum);
									$monthName = $dateObj->format('F'); // March
									$date = $dt->tanggal." ".$monthName." ".$dt->tahun;
									?>
									<td>{{$date}}</td>
									<td>{{$dt->acara}} </td>
									<td>{{ $dt->kegiatan }}</td>
									<td>
										@include('view_admin.agenda.update')
										@include('view_admin.agenda.delete')
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		@include('admin.footer');