   @include('new_layout.header')
   @include('new_layout.sidebar')
   <div class="dashboard-wrapper">
   	<div class="dashboard-ecommerce">
   		<div class="container-fluid dashboard-content ">
   			<form action="/admin/agenda/fnc/insert" method="post" enctype="multipart/form-data" role="form">
   				<input type="hidden" name="_token" value="{{ csrf_token() }}">
   		
   				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
   					@if ($message = Session::get('error'))
   					<div class="alert alert-danger alert-block">
   						<button type="button" class="close" data-dismiss="alert">×</button> 
   						<strong>{{ $message }}</strong>
   					</div>
   					@endif
   					<div class="card">
   						<h5 class="card-header">Tambah Agenda</h5>
   						<div class="card-body">
   							<form class="needs-validation" novalidate>
   								<div class="row">
   									<div class="col-md-6">
   										<label for="validationCustom01">Waktu</label>
   										<input type="date" class="form-control" id="date" name="date" placeholder="Bulan" required>
   										<div class="valid-feedback">
   											Looks good!
   										</div>
   									</div>
   									<div class="col-md-6">
   										<label for="validationCustom01">Acara</label>
   										<input type="text" class="form-control" id="acara" name="acara" placeholder="Acara">
   										<div class="valid-feedback">
   											Looks good!
   										</div>
   									</div>
   									<div class="col-md-12">
   										<div class="form-group">
   											<label>Kegiatan</label>
   											<input type="text" class="form-control" id="kegiatan" name="kegiatan" placeholder="Kegiatan">
   										</div>
   									</div>

   								</div>
   								<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 ">
   									<button class="btn-sm btn-rounded btn-primary" type="submit">Simpan</button>
   								</div>
   							</div>
   						</form>
   					</div>
   				</div>
   			</div>
   		</form>
   		@include('new_layout.footer')
   	</div>
   </div>