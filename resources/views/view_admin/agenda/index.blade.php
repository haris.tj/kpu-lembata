@include('new_layout.header')
@include('new_layout.sidebar')
<div class="dashboard-wrapper">
	<div class="container-fluid dashboard-content">
		<!-- ============================================================== -->
		<!-- pageheader -->
		<!-- ============================================================== -->
		<div class="row">
			<!-- ============================================================== -->
			<!-- data table multiselects  -->
			<!-- ============================================================== -->
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				
				<div class="card">
					<div class="card-header">
						<div class="row form-group">
							<div class="col-sm-6">
								{{-- <button class="btn-sm btn-rounded btn-primary"><i class="ti-plus"></i> Tambah</button> --}}
								{{-- <a href="/admin/agenda/insert" class="btn-lg btn-rounded btn-primary"><i class="ti-plus"></i> Tambah</a> --}}
								<button class="btn-rounded btn-sm btn-primary" onclick="tambah()">
									<i class="ti-plus">  Tambah</i>
								</button>
							</div>
							<div class="col-sm-6 text-right">
								<h2>Data Agenda</h2>
							</div>
						</div>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="example3" class="table table-striped table-bordered" style="width:100%">
								<thead>
									<tr>
										<th>No</th>
										<th>Waktu</th>
										<th>Acara</th>
										<th>Kegiatan</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php $no = 1;?>
									@foreach($agenda as $dt)
									<tr>
										<td>{{ $no++ }}</td>
										<?php
										$monthNum  = $dt->bulan;
										$dateObj   = DateTime::createFromFormat('!m', $monthNum);
									$monthName = $dateObj->format('F'); // March
									$date = $dt->tanggal." ".$monthName." ".$dt->tahun;
									?>
									<td>{{$date}}</td>
									<td>{{$dt->acara}} </td>
									<td>{{ $dt->kegiatan }}</td>
									<td>
										@include('view_admin.agenda.update')
										@include('view_admin.agenda.delete')
									</td>
								</tr>
								@endforeach
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- ============================================================== -->
			<!-- end data table multiselects  -->
			<!-- ============================================================== -->
		</div>
		<!-- ============================================================== -->
		<!-- end pageheader -->
		<!-- ============================================================== -->

	</div>
	@include('new_layout.footer')
	<script type="text/javascript">
		function tambah() {
			window.location = "/admin/agenda/insert";
		}
	</script>
