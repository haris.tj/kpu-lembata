   @include('new_layout.header')
   @include('new_layout.sidebar')
   <div class="dashboard-wrapper">
   	<div class="dashboard-ecommerce">
   		<div class="container-fluid dashboard-content ">
        <form action="/admin/artikel/update" method="post" enctype="multipart/form-data" role="form">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
         {{--  <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.0.15/tinymce.min.js" referrerpolicy="origin"></script>
          <script type="text/javascript">
            tinymce.init({selector:'textarea'});
          </script> --}}
          @foreach($artikel as $dt)
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
              <h5 class="card-header">Update Artikel</h5>
              <div class="card-body">
                <form class="needs-validation" novalidate>
                  <div class="row">
                    <input type="hidden" name="id" id="id" value="{{$dt->id}}">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                      <label for="validationCustom01">Judul Artikel</label>
                      <input type="text" class="form-control" id="judul" value="{{ $dt->judul }}" name="judul" placeholder="Judul Artikel" required>
                      <div class="valid-feedback">
                        Looks good!
                      </div>
                    </div>
                    <div class="col-md-12">
                     <div class="form-group">
                       <label>Artikel</label>
                       <textarea name="konten" id="konten" class="ckeditor" rows="15" placeholder="Konten Artikel">{{$dt->isi}}</textarea>
                     </div>
                   </div>
                   @endforeach
                 </div>
                 <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 ">
                  <button class="btn-rounded btn-sm btn-primary" type="submit">Simpan</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </form>
    @include('new_layout.footer')
  </div>
</div>