@include('layout.header')

<div class="container">
	<div class="row">
		<h3>Komisioner</h3>
		<hr style="height:2px;border-width:0;color:gray;background-color:gray">
	</div>
</div>

<div class="container">
	<div class="row-fluid form-group">
		@foreach($data as $dt)
		<div class="col-md-4">
			<div class="image-wrap">
				<div class="image-popup">
					<?php
					$q = $dt->tgl_lahir;
					$p = $dt->tempat_lahir;
					$ps = explode("-", $q);
					$t = $ps[0];
					$b = $ps[1];
					$h = $ps[2];
					$nama_bulan = date("F", mktime(0, 0, 0, $b, 10));
					$ttl = $p." , ".$h." - ".$nama_bulan." - ".$t;
					?>

					<p><small>
						Nama    : {{$dt->nama}}<br>
						Jabatan : {{$dt->jabatan}}<br>
						TTL  	  : {{$ttl}}<br>
						Jabatan : {{$dt->jabatan}}<br>
						@if(!empty($dt->devisi))
						Devisi : {{$dt->devisi}}<br>
						@endif
						Korwil : {{$dt->korwil}}<br>
						Alamat  : {{$dt->alamat}}<br>
						Email	  : {{$dt->email}}<hr>
					</small></p>

				</div>
				<img src="public/komisioner/{{$dt->gambar}}" height="380" width="347" class="rounded-corners">
			</div>
		</div>
		@endforeach
	</div>
</div>

@include('layout.footer')
