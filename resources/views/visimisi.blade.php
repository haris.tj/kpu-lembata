
@include('layout.header')
<!-- container -->
<div class="container">
	<!-- row -->
	<div class="row">
		<div class="col-md-12">
			<!-- row -->
			<div class="row form-group">
				<div class="col-md-12">
					<div class="section-title">
						<h2 class="title">Visi - Misi</h2>
					</div>
				</div>


				<div class="container-fluid">
					<div class="row form-group">
						<div class="col-md-1"></div>
						<div class="col-md-4 col-xs-12">
							<div class="pull-left">
								<img src="{{url('image/visi.png')}}" width="370" height="300">
							</div>
						</div>
						@foreach($data as $dt)
						<div class="col-md-6">
							<h3 style="color: #ee4266;">VISI</h3><hr>
							{!! $dt->visi !!}
						</div>
						
					</div>
					@endforeach
					<div class="col-md-1"></div>
					<div class="row form-group">
						@foreach($data as $dt)
						<h3 style="color: #ee4266;">MISI</h3><hr>
						<div class="col-md-1"></div>
						<div class="col-md-7">{!! $dt->misi !!}</div>
						@endforeach
						<div class="col-md-4 col-xs-12">
							<div class="pull-right">
								<img src="{{url('image/misi.png')}}" width="370" height="300">
							</div>
						</div>
						<div class="col-md-1"></div>
					</div>
				</div>


			</div>


		</div>
	</div>
</div>
<!-- /row -->
</div>
<!-- /container -->
@include('layout.footer')