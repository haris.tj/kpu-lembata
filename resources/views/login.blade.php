<!doctype html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Login</title>
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="{{url('assetz/vendor/bootstrap/css/bootstrap.min.css')}}">
	<link href="{{url('assetz/vendor/fonts/circular-std/style.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="{{url('assetz/libs/css/style.css')}}">
	<link rel="stylesheet" href="{{url('assetz/vendor/fonts/fontawesome/css/fontawesome-all.css')}}">
	<style>
		html,
		body {
			height: 100%;
		}

		body {
			display: -ms-flexbox;
			display: flex;
			-ms-flex-align: center;
			align-items: center;
			padding-top: 40px;
			padding-bottom: 40px;
		}
	</style>
</head>

<body>
	<!-- ============================================================== -->
	<!-- login page  -->
	<!-- ============================================================== -->
	<div class="splash-container">
		<div class="card ">
			<div class="card-header text-center">
			{{-- 	<a href="../index.html">
					<img class="logo-img" src="../assets/images/logo.png" alt="logo">
				</a> --}}
				<h2 class="text-center">LOGIN</h2>
				<span class="splash-description">Please enter your user information.</span>
				@if ($message = Session::get('error'))
				<div class="alert alert-danger alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
					<strong>{{ $message }}</strong>
				</div>
				@elseif ($message = Session::get('success'))
				<div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
					<strong>{{ $message }}</strong>
				</div>
				@endif
			</div>
			<div class="card-body">
				<form action="/admin/login/act" method="post" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="form-group">
						<input class="form-control form-control-lg" id="username" name="username" type="text" placeholder="Username" autocomplete="off">
					</div>
					<div class="form-group">
						<input class="form-control form-control-lg" id="password" type="password" name="password" placeholder="Password">
					</div>
					<div class="form-group">
						<label class="custom-control custom-checkbox">
							<input class="custom-control-input" type="checkbox"><span class="custom-control-label">Remember Me</span>
						</label>
					</div>
					<button type="submit" class="btn btn-primary btn-lg btn-block">Sign in</button>
				</form>
			</div>
			<div class="card-footer bg-white p-0  ">
				<div class="card-footer-item card-footer-item-bordered">
					<a href="/admin/register" class="footer-link">Create An Account</a></div>
					{{-- <a href="#" class="footer-link">Forgot Password</a> --}}
				</div>
			</div>
		</div>

		<!-- ============================================================== -->
		<!-- end login page  -->
		<!-- ============================================================== -->
		<!-- Optional JavaScript -->
		<script src="{{url('assetz/vendor/jquery/jquery-3.3.1.min.js')}}"></script>
		<script src="{{url('assetz/vendor/bootstrap/js/bootstrap.bundle.js')}}"></script>
	</body>

	</html>