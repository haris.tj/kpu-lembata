  <!-- ============================================================== -->
  <!-- footer -->
  <!-- ============================================================== -->
  @if(empty(Session::get('userid')))
  <script type="text/javascript">
            window.location = "/admin/login";//here double curly bracket
          </script>
          @endif
          <div class="footer">
            <div class="container-fluid">
              <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                  Copyright &copy;<script>document.write(new Date().getFullYear());</script> KPU Kabupaten Lembata
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                  <div class="text-md-right footer-links d-none d-sm-block">
                    <a href="javascript: void(0);">About</a>
                    <a href="javascript: void(0);">Support</a>
                    <a href="javascript: void(0);">Contact Us</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- ============================================================== -->
          <!-- end footer -->
          <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- end main wrapper -->
        <!-- ============================================================== -->
      </div>
      <!-- ============================================================== -->
      <!-- end main wrapper -->
      <!-- ============================================================== -->
      <!-- Optional JavaScript -->
      {{-- <script src="../assets/vendor/jquery/jquery-3.3.1.min.js"></script> --}}
      <script src="{{url('assetz/vendor/jquery/jquery-3.3.1.min.js')}}"></script>

      {{-- <script src="../assets/vendor/bootstrap/js/bootstrap.bundle.js"></script> --}}
      <script src="{{url('assetz/vendor/bootstrap/js/bootstrap.bundle.js')}}"></script>

      {{-- <script src="../assets/vendor/slimscroll/jquery.slimscroll.js"></script> --}}
      <script src="{{url('assetz/vendor/slimscroll/jquery.slimscroll.js')}}"></script>

      {{-- <script src="../assets/libs/js/main-js.js"></script> --}}
      <script src="{{url('assetz/libs/js/main-js.js')}}"></script>
      <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
      <script src="{{url('assetz/vendor/datatables/js/dataTables.bootstrap4.min.js')}}"></script>
      <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
      <script src="{{url('assetz/vendor/datatables/js/buttons.bootstrap4.min.js')}}"></script>
      <script src="{{url('assetz/vendor/datatables/js/data-table.js')}}"></script>
      <script src="{{url('assetz/ckeditor/ckeditor.js')}}"></script>
      <script src="https://cdn.datatables.net/rowgroup/1.0.4/js/dataTables.rowGroup.min.js"></script>
      <script src="https://cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js"></script>
      <script src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
      {{-- @include('sweetalert::alert', ['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"]) --}}
      @include('sweetalert::alert')

    </body>

    </html>