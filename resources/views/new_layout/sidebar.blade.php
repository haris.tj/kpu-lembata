 <!-- ============================================================== -->
 <!-- left sidebar -->
 <!-- ============================================================== -->
 @if(empty(Session::get('userid')))
 <script type="text/javascript">
            window.location = "/admin/login";//here double curly bracket
        </script>
        @endif
        <div class="nav-left-sidebar sidebar-dark">
            <div class="menu-list">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="d-xl-none d-lg-none" href="#">Dashboard</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav flex-column">
                            <li class="nav-divider">
                                Menu
                            </li>
                            {{-- PUBLIKASI --}}
                            <li class="nav-item">
                               <a class="nav-link" href="/admin" ><i class="fa fa-fw far fa-handshake"></i>Dashboard <span class="badge badge-success">6</span></a>
                               <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-5" aria-controls="submenu-5"><i class="fas fa-fw  fas fa-globe"></i>Publikasi</a>
                               <div id="submenu-5" class="collapse submenu" style="">
                                <ul class="nav flex-column">
                                 <li class="nav-item">
                                    <a class="nav-link" href="{{url('admin/artikel')}}">Artikel</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/admin/berita">Berita</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('admin/agenda')}}">Agenda</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('admin/album')}}">Galery Photo</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    {{-- HOME --}}
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-6" aria-controls="submenu-6"><i class="fas fa-fw fa-home"></i>Home</a>
                        <div id="submenu-6" class="collapse submenu" style="">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('admin/visi-misi')}}">Visi Misi</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('admin/komisioner')}}">Komisioner KPU</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('admin/user/home')}}">Profil Sekertariat</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('admin/struktur')}}">Struktur Organisasi</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    {{--  --}}
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('admin/hukum')}}"><i class="fa fa-fw fa-rocket"></i>Produk Hukum</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('admin/info')}}"><i class="fa fa-fw fas fa-info-circle"></i>Info PPID</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('admin/pemilih')}}"><i class="fa fa-fw fas fa-check"></i>Data Pemilih</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('admin/kritik-saran')}}"><i class="fa fa-fw fas fa-book"></i>Kritik & Saran</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</div>
<!-- ============================================================== -->
<!-- end left sidebar -->
    <!-- ============================================================== -->