<!doctype html>
<html lang="en">


<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>KPU Lembata</title>
    <link rel="stylesheet" href="{{url('assetz/vendor/bootstrap/css/bootstrap.min.css')}}">
    <link href="{{url('assetz/vendor/fonts/circular-std/style.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{url('assetz/libs/css/style.css')}}">
    <link rel="stylesheet" href="{{url('assetz/vendor/fonts/fontawesome/css/fontawesome-all.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('assetz/vendor/datatables/css/dataTables.bootstrap4.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('assetz/vendor/datatables/css/buttons.bootstrap4.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('assetz/vendor/datatables/css/select.bootstrap4.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('assetz/vendor/datatables/css/fixedHeader.bootstrap4.css')}}">
    <link rel="stylesheet" href="{{url('assetz/vendor/fonts/themify-icons/themify-icons.css')}}">

</head>
<style type="text/css">
    .pagination li{
        float: left;
        list-style-type: none;
        margin:8px;
    }
</style>
@if(empty(Session::get('userid')))
<script type="text/javascript">
            window.location = "/admin/login";//here double curly bracket
        </script>
        @endif
        <body>
            <!-- ============================================================== -->
            <!-- main wrapper -->
            <!-- ============================================================== -->
            <div class="dashboard-main-wrapper">
                <!-- ============================================================== -->
                <!-- navbar -->
                <!-- ============================================================== -->
                <div class="dashboard-header">
                    <nav class="navbar navbar-expand-lg bg-white fixed-top">
                        <a class="navbar-brand" href="/admin">KPU Lembata</a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav ml-auto navbar-right-top">
                                <li class="nav-item">
                                    <div id="custom-search" class="top-search-bar">
                                        <input class="form-control" type="text" placeholder="Search..">
                                    </div>
                                </li>
                                
                                <li class="nav-item dropdown nav-user">
                                    <a class="nav-link nav-user-img" href="#" id="navbarDropdownMenuLink2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{url('assetz/images/profile.png')}}" alt="" class="user-avatar-md rounded-circle"></a>
                                    <div class="dropdown-menu dropdown-menu-right nav-user-dropdown" aria-labelledby="navbarDropdownMenuLink2">
                                        <div class="nav-user-info">
                                            <h5 class="mb-0 text-white nav-user-name">{{Session::get('username')}}</h5>
                                        </div>
                                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#updateAccount"><i class="fas fa-user mr-2"></i>Account</a>
                                        <a class="dropdown-item" href="/admin/logout"><i class="fas fa-power-off mr-2"></i>Logout</a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
                <!-- ============================================================== -->
                <!-- end navbar -->
                <!-- ============================================================== -->

                {{-- MODAL --}}

          {{--       <button class="btn-xs btn-rounded btn-warning" data-toggle="modal" data-target="#updateAccount">
                    <i class="ti-pencil"></i>
                </button> --}}
                <div class="modal fade" id="updateAccount" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4>Profile</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times</button>
                            </div>
                            <form action="/admin/respass" method="post" enctype="multipart/form-data" role="form">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="id" value="{{Session::get('userid')}}" id="id">
                                {{method_field('patch')}}
                                <div class="modal-body">
                                   <div class="card card-fluid">
                                    <!-- .card-body -->
                                    <div class="card-body text-center">
                                        <!-- .user-avatar -->
                                        <a href="#" class="user-avatar my-3">
                                            <img src="{{url('assetz/images/profile.png')}}" alt="User Avatar" class="card-img" width="356" height="256">
                                        </a>
                                        <!-- /.user-avatar -->
                                        <h3 class="card-title mb-2 text-truncate">
                                            <a>{{Session::get('username')}}</a>
                                        </h3>
                                        <h6 class="card-subtitle text-muted mb-3">{{Session::get('email')}}</h6>
                                        {{-- <p class="text-left">Password</p> --}}
                                        <input type="password" name="password" placeholder="New Password" class="form-control">
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn-rounded btn-info btn-sm">Save</button>
                                <button type="button" class="btn-rounded btn-default btn-sm" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                function reply_click()
                {   var id = event.srcElement.id;
                    var x = document.getElementById('id').value = id;
                }
            </script>
    {{-- END MODAL --}}