
@include('layout.header');
<!-- container -->
<div class="container">
	<!-- row -->
	<div class="row">
		<div class="col-md-12">
			<!-- row -->
			<div class="row">
				<div class="col-md-12">
					<div class="section-title">
						<h2 class="title">Album</h2>
					</div>
				</div>
				<!-- post -->
				@foreach($album as $dt)
				<div class="col-md-4">
					<div class="post">
						<a href="/album/{{$dt->id_album}}" class="post-img">
							<img src="{{url('lembata/img/read.jpg')}}" alt="">
						</a>
						<div class="post-body text-center">
							<div class="post-category">
								<a href="#">{{$dt->nama_album}}</a>
							</div>
							<h3 class="post-title"><a href="#"></a></h3>
							<ul class="post-meta">
								<li>{{$dt->waktu}}</li>
							</ul>
						</div>
					</div>
				</div>
				@endforeach
			</div>
		</div>
	</div>
	<!-- /row -->
</div>
<!-- /container -->
@include('layout.footer');