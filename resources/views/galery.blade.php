@include('layout.header')
<style type="text/css">
  body {
    padding-top: 20px;
    
  }

  .btn-default {
    top: 25%;
    left:25%; 
    color: black; 
  } 
</style>
<div class="container">
  <!-- row -->
  <div class="row">
    <div class="col-md-12">
      <!--  -->
      <!--  -->
      <div class="row">
        <div class="col-md-12">
          <div class="section-title">
            <h2 class="title">Galery</h2>
          </div>
          {{-- <button class="btn btn-primary btn-round" data-toggle="modal" data-target=".bs-example-modal-lg">Tampilan Slider</button> --}}
          <button class="btn-primary btn-rounded btn-sm" data-toggle="modal" data-target="#myModal">Tampilkan Slider</button>

          <hr>
        </div>
        <!--  -->
        @foreach($data as $dt)
        <?php
        $nama = $dt->nama_gambar;
        $exp = explode('|', $nama);
        ?>
        @for($x=0; $x<count($exp); $x++)
        <div class="col-md-4">
          <div class="post">
            <div class="post-img">
              <img src="{{url('image/'.$exp[$x])}}" alt="">
            </div>
          </div>
        </div>
        @endfor
        @endforeach

        <!--begin modal window-->

        <!--  -->
      </div>
    </div>
  </div>
</div>
@include('layout.footer')

        <div class="modal fade" id="myModal">

          <div class="modal-dialog modal-lg">

            <div class="modal-content">

              <div class="modal-header">

                <div class="pull-left">
                  My Galery
                </div>

                <button type="button" class="close" data-dismiss="modal" title="Close">CLose</button>

              </div>

              <div class="clearfix"></div>

              <div class="modal-body">

                <!--CAROUSEL CODE GOES HERE-->

                <div id="myCarousel" class="carousel slide" data-interval="false">

                  <div class="carousel-inner">

                    @foreach($data as $dt)

                    <div class="item active">
                      <img src="http://placehold.it/1200x600/555/000&text={{$dt->nama_album}}" alt="">
                    </div>
                    @for($x=0; $x<count($exp); $x++)
                    <div class="item text-center"> 
                      <img src="{{url('image/'.$exp[$x])}}" alt="">
                    </div>
                    @endfor
                  </div>
                  @endforeach

                </div>

                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"></a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"></a>
              </div>
            </div>
          </div>
        </div>