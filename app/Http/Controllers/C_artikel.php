<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Session;

class C_artikel extends Controller{
    public function index(){
        $data = DB::table('t_artikel')->orderBy('id', 'desc')->paginate(6);
        return view('view_admin/HomeArtikel',['artikel' => $data]);
    }

    public function create(Request $request){
        $date = date('Y-m-d H:i');
        $judul = $request->input('judul');
        $isi = $request->input('konten');
        $data = array(
            'tanggal' => $date, 
            'judul' => $judul,
            'isi'=> $isi,
            'pengirim'  => Session::get('username'), //get data username from session
            'visits'    => 0, //counter visitor start from 0
            'admin' => Session::get('userid'), //id user
        );
        if (DB::table('t_artikel')->insert($data)) {
            return redirect('/admin/artikel')->with(['success' => 'Data Berhasil Ditambahkan']);
        }else{
            return redirect('/admin/artikel/insert');
        }
    }

    public function store(Request $request){

    }
    public function show($id){
        $data = DB::table('t_artikel')->where('id',$id)->get();
        foreach ($data as $key) {
            $c = $key->visits;
        }
        $counter = array('visits' => $c+1);
        DB::table('t_artikel')->where('id',$id)->update($counter);
        return view('view_admin/DetailArtikel',['detail' => $data]);

    }
    public function edit($id){
        $data = DB::table('t_artikel')->where('id',$id)->get();
        return view('view_admin/EditArtikel',['artikel' => $data]);
    }

    public function update(Request $request){
        $id = $request->input('id');
        $judul = $request->input('judul');
        $isi = $request->input('konten');
        $date = date('Y-m-d H:i');
        $data = array(
            'tanggal' => $date, 
            'judul' => $judul,
            'isi'=> $isi,
            'pengirim'  => Session::get('username'), //get data username from session
            'admin' => Session::get('userid'), //id user
        );
        DB::table('t_artikel')->where('id',$id)->update($data);
        return redirect('/admin/artikel')->with(['success' => 'Data Berhasil Dirubah']);;
    }
    public function destroy($id){
        DB::table('t_artikel')->where('id',$id)->delete();
        return redirect('/admin/artikel')->with(['success' => 'Data Berhasil DiHapus']);;
    }
    public function userview(){
        $data = DB::table('t_artikel')->orderBy('id', 'desc')->paginate(3);
        return view('/artikel',['artikel' => $data]);
    }
    public function usrDetArtikel($id){
        $data = DB::table('t_artikel')->where('id',$id)->get();
        $data2 = DB::table('t_artikel')->where('id','!=',$id)->limit(3)->get();
        $arr = array(
            'detail'    => $data,
            'detail2'   => $data2
        );
        foreach ($data as $key) {
            $c = $key->visits;
        }
        $counter = array('visits' => $c+1);
        DB::table('t_artikel')->where('id',$id)->update($counter);
        return view('artikelDetail',$arr);

    }
}
