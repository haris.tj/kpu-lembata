<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class C_login extends Controller{
    //

	public function index(){
		return view('login');
	}

	public function login(Request $Request){
		$usename  = $Request->input('username');
		$password = $Request->input('password');
		$data = DB::table('user')->where('username',$usename)->first();
		if ($data) {
			if (Hash::check($password,$data->password)) {
				Session::put('userid',$data->userid);
				Session::put('username',$data->username);
				Session::put('email',$data->email);
				return redirect('/admin');
			}else{
				return redirect('/admin/login')->with(['error' => 'Login Gagal']);
			}
		}else{
			return redirect('/admin/login')->with(['error' => 'Login Gagal']);
		}
	}

	public function create(Request $Request){
		$username = $Request->input('username');
		$password = $Request->input('password');
		$email = $Request->input('email');
		$ip = request()->ip();
		$waktu = date('Y-m-d H:i');
		$kunjung = 4;
		$status = 1;
		$data = array(
			'username'	=> $username,
			'password'	=> bcrypt($password),
			'email'	=> $email,
			'ip'	=> $ip,
			'waktu' => $waktu,
			'kunjung'	=> $kunjung,
			'status'	=> $status
		);
		if (DB::table('user')->insert($data)) {
			// echo "insert success";
			return redirect('/admin/login')->with(['success' => 'Register Berhasil']);
		}
	}
	public function gettoken(){
		return csrf_token();
	}

	public function update(Request $req){
		$id = $req->input('id');
		$arr = array('password' => bcrypt($req->input('password')));
		if (DB::table('user')->where('userid',$id)->update($arr)) {
			Session::flush();
			return redirect('/admin/login')->with(['success' => 'Success, Login Kembali']);
		}else{
			return redirect()->back();
		}
	}
	public function registerview(){
		return view('insert');
	}
	public function logout(){
		Session::flush();
		return redirect('/');
	}
}
