<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use DateTime;

class C_agenda extends Controller{

	public function index(){
		$data = DB::table('t_agenda')->get();
		return view('view_admin/agenda/index',['agenda' => $data]);
	}

	public function insert(Request $request){
		$date = $request->input('date');
		$acara = $request->input('acara');
		$kegiatan = $request->input('kegiatan');
		// return dd($date);
		$datetime = new DateTime($date);
		$t = $datetime->format('Y');
		$b = $datetime->format('n');
		$h = $datetime->format('d');

		$data = array(
			'bulan'	=> $b,
			'tahun' => $t,
			'tanggal'	=> $h,
			'acara'	=> $acara,
			'kegiatan'	=> $kegiatan, 
		);
		if (DB::table('t_agenda')->insert($data)) {
			return redirect('/admin/agenda')->with(['success' => 'Tambah Data Berhasil']);;
		}
	}

	public function detail($id){
		$data = DB::table('t_agenda')->where('id_agenda',$id)->first();
		return view('/admin/agenda',['agenda' => $data]);
	}

	public function delete($id){
		DB::table('t_agenda')->where('id_agenda',$id)->delete();
		return redirect('/admin/agenda')->with(['success' => 'Data Berhasil DiHapus']);;
	}

	public function view_agenda(){
		return view('agenda');
	}
	public function getagenda($b,$t){
		$data = DB::table('t_agenda')->where('bulan',$b)->where('tahun',$t)->get();
		echo json_encode($data);
	}

	// public function edit($id){
	// 	$data = DB::table('t_sim')->where('id_sim',$id)->get();
	// 	return view('sim/update',['data' => $data]);
	// }
	public function updatedata(Request $req){
		$id = $req->input('id');
		$date = $req->input('date');
		$acara = $req->input('acara');
		$kegiatan = $req->input('kegiatan');

		$datetime = new DateTime($date);
		$t = $datetime->format('Y');
		$b = $datetime->format('n');
		$h = $datetime->format('d');

		$data = array(
			'bulan'	=> $b,
			'tahun' => $t,
			'tanggal'	=> $h,
			'acara'	=> $acara,
			'kegiatan'	=> $kegiatan, 
		);
		if (DB::table('t_agenda')->where('id_agenda',$id)->update($data)) {
			return redirect('/admin/agenda')->with(['success' => 'Ubah Data Berhasil']);
		}
	}

}
