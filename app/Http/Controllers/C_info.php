<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class C_info extends Controller{

	public function index(){
		$data = DB::table('t_info_new')->get();
		return view('view_admin/info/home',['data' => $data]);
	}

	public function insert(Request $request){
		$data = array(
			'tgl'	=> date('Y-m-d'),
			'nama_pemohon'	=> $request->input('nama_pemohon'),
			'alamat'	=> $request->input('alamat'),
			'informasi'	=> $request->input('informasi')
		);
		if (DB::table('t_info_new')->insert($data)) {
			return redirect('/admin/info')->with(['success' => 'Data Berhasil Ditambahkan']);
		}
//
	}

	public function detail($id){
		$data = DB::table('t_info_new')->where('id_info',$id)->get();
		return view('view_admin/info/update',['data' => $data]);
	}

	public function update(Request $request){
		
		$id = $request->input('id');
		$data = array(
			'tgl'	=> date('Y-m-d'),
			'nama_pemohon'	=> $request->input('nama_pemohon'),
			'alamat'	=> $request->input('alamat'),
			'informasi'	=> $request->input('informasi')
		);
		DB::table('t_info_new')->where('id_info',$id)->update($data);
		return redirect('/admin/info')->with(['success' => 'Data Berhasil Dirubah']);
	}

	public function destroy($id){
		DB::table('t_info_new')->where('id_info',$id)->delete();
		return redirect('/admin/info')->with(['success' => 'Data Berhasil DiHapus']);;
	}

	public function uview(){
		$data = DB::table('t_info_new')->get();
		return view('info',['data' => $data]);
	}
}
