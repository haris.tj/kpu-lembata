<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Session; 

class Controller extends BaseController
{
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	public function __construct(){
		$this->middleware(function($request,$next){
			if (Session::get("success")) {
				toast(Session::get("success"),'success');
			}elseif(Session::get('error')){
				toast(Session::get("error"),'error');
			}
				// lert::error('Error Title', 'Error Message');
			return $next($request);
		});
	}
}
