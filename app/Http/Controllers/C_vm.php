<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class C_vm extends Controller
{
	public function index(){
		$data = DB::table('t_visimisi')->get();
		return view('view_admin/visimisi/home',['data' => $data]);
	}
	public function insertview(){
		return view('view_admin/visimisi/insert');
	}

	public function insert(Request $request){
		$data = array(
			'visi'	=> $request->input('visi'),
			'misi' => $request->input('misi')
		);
		if (DB::table('t_visimisi')->insert($data)) {
			return redirect('/admin/visi-misi')->with(['success' => 'Tambah Data Berhasil']);;
		}
	}

	public function detail($id){
		$data = DB::table('t_visimisi')->where('id',$id)->get();
		return view('view_admin/visimisi/update',['data' => $data]);
	}

	public function delete($id){
		DB::table('t_visimisi')->where('id',$id)->delete();
		return redirect('/admin/visi-misi')->with(['success' => 'Data Berhasil DiHapus']);;
	}

	public function userview(){
		$data = DB::table('t_visimisi')->get();
		return view('visimisi',['data' => $data]);
	}

	public function update(Request $request){
		$id = $request->input('id');
		$data = array(
			'visi'	=> $request->input('visi'),
			'misi' => $request->input('misi')
		);
		if (DB::table('t_visimisi')->where('id',$id)->update($data)) {
			return redirect('/admin/visi-misi')->with(['success' => 'Ubah Data Berhasil']);
		}
	}

}
