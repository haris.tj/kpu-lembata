<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\File\UploadedFile;


class C_berita extends Controller{

	public function index(){
		$data = DB::table('t_berita')->orderBy('id', 'desc')->paginate(6);
		return view('view_admin/berita/Home',['berita' => $data]);
	}

	public function create(Request $request){
		$validator = Validator::make($request->all(), [
			'gambar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
		]);

		if ($validator->fails()) {
			return redirect('/admin/berita/insert');
			toast("Upload Gambar Gagal",'error');
		}

		$file = $request->file('gambar');
		if (empty($file) || $file == "" || $file == 0 ) {

			$date = date('Y-m-d');
			$time = date('H:i:s');
			$subject = $request->input('judul');
			$isi = $request->input('konten');
			$data = array(
				'isi'=> $isi,
				'subject' => $subject,
				'gambar' => null,
            'pengirim' => Session::get('userid'), //id user
            'posttime' => $time,
            'postdate' => $date,
            'visits'    => 0, //counter visitor start from 0
        );
			if (DB::table('t_berita')->insert($data)) {
				return redirect('/admin/berita')->with(['success' => 'Data Berhasil Ditambahkan']);
			}else{
				return redirect('/admin/berita/insert');
			}
		}else{
			$destinationPath = "public/uploads";
			$fileName = $file->getClientOriginalName();
			$request->file('gambar')->move($destinationPath, $fileName);

			$date = date('Y-m-d');
			$time = date('H:i:s');
			$subject = $request->input('judul');
			$isi = $request->input('konten');
			$data = array(
				'isi'=> $isi,
				'subject' => $subject,
				'gambar' => $fileName,
            'pengirim' => Session::get('userid'), //id user
            'posttime' => $time,
            'postdate' => $date,
            'visits'    => 0, //counter visitor start from 0
        );
			if (DB::table('t_berita')->insert($data)) {
				return redirect('/admin/berita')->with(['success' => 'Data Berhasil Ditambahkan']);
			}else{
				return redirect('/admin/berita/insert');
			}
		}
//
	}

	public function show($id){
		$data = DB::table('t_berita')->where('id',$id)->get();
		foreach ($data as $key) {
			$c = $key->visits;
		}
		$counter = array('visits' => $c+1);
		DB::table('t_berita')->where('id',$id)->update($counter);
		return view('view_admin/berita/detail',['detail' => $data]);

	}
	public function edit($id){
		$data = DB::table('t_berita')->where('id',$id)->get();
		return view('view_admin/berita/edit',['artikel' => $data]);
	}

	public function update(Request $request){
		$validator = Validator::make($request->all(), [
			'gambar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
		]);

		if ($validator->fails()) {
			//return with(['error' => 'Upload Gambar Gagal.!']);
			return redirect('/admin/berita/'.$request->input('id'));
			// ->with(['error' => 'Upload Gambar Gagal.!']);
			toast("Upload Gambar Gagal",'error');
		}
		$file = $request->file('gambar');
		if (empty($file) || $file == "" || $file == null) {
			$id = $request->input('id');
			$date = date('Y-m-d');
			$time = date('H:i:s');
			$subject = $request->input('judul');
			$isi = $request->input('konten');
			$data = array(
				'isi'	   => $isi,
				'subject'  => $subject,
           		'pengirim' => Session::get('userid'), //id user
           		'posttime' => $time,
           		'postdate' => $date,
           	);
			DB::table('t_berita')->where('id',$id)->update($data);
			return redirect('/admin/berita')->with(['success' => 'Data Berhasil Dirubah']);		
		}else{
			//img upload
			$destinationPath = "public/uploads";
			$fileName = $file->getClientOriginalName();
			$request->file('gambar')->move($destinationPath, $fileName);

			$id = $request->input('id');
			$date = date('Y-m-d');
			$time = date('H:i:s');
			$subject = $request->input('judul');
			$isi = $request->input('konten');
			$data = array(
				'isi'	   => $isi,
				'subject'  => $subject,
				'gambar'   => $fileName,
           		'pengirim' => Session::get('userid'), //id user
           		'posttime' => $time,
           		'postdate' => $date,
           	);
			DB::table('t_berita')->where('id',$id)->update($data);
			return redirect('/admin/berita')->with(['success' => 'Data Berhasil Dirubah']);
		}

	}

	public function destroy($id){
		DB::table('t_berita')->where('id',$id)->delete();
		return redirect('/admin/berita')->with(['success' => 'Data Berhasil DiHapus']);;
	}

	public function userview(){
		$data = DB::table('t_artikel')->orderBy('id', 'desc')->limit(3)->get();
		$data2 = DB::table('t_berita')->orderBy('id', 'desc')->limit(3)->get();
		$arr = array(
			'artikel'	=> $data,
			'berita'	=> $data2
		);
		return view('/home_view',$arr);
	}

	public function beritaview(){
		$data = DB::table('t_berita')->orderBy('id', 'desc')->paginate(3);
		$arr = array(
			'artikel'	=> $data,
		);
		return view('/berita',$arr);
	}

	public function usrDetBerita($id){
		$data = DB::table('t_berita')->where('id',$id)->get();
		$data2 = DB::table('t_berita')->where('id','!=',$id)->limit(3)->get();
		$arr = array(
			'detail'    => $data,
			'detail2'   => $data2
		);
		foreach ($data as $key) {
			$c = $key->visits;
		}
		$counter = array('visits' => $c+1);
		DB::table('t_berita')->where('id',$id)->update($counter);
		return view('beritaDetail',$arr);

	}

}
