<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;


class C_galery extends Controller
{
    //
	public function index(){
		$data = DB::table('t_album')->get();
		return view('view_admin/galery/home',['data' => $data]);
	}

	public function insert(Request $req){

		$name =  $req->input('nama');
		$waktu	= date('Y-m-d');
		$data = array(
			'nama_album'	=> $name,
			'waktu'			=> $waktu
		);
		if (DB::table('t_album')->insert($data)) {
			return redirect('/admin/album')->withSuccess('Data Berhasil Ditambahkan');

		}
	}

	public function insertIMG(Request $req){
		$id = $req->input('id');
		$images=array();
		if($files=$req->file('images')){
			foreach($files as $file){
				$name=$file->getClientOriginalName();
				$file->move('image',$name);
				$images[]=$name;
			}
		}
		$data = array(
			'id_album'	=> $id,
			'nama_gambar'	=> implode("|",$images)
		);

		if (DB::table('t_galery')->insert($data)) {
			return redirect('/admin/album')->with(['success' => 'Data Berhasil Ditambahkan']);
		}
	}

	public function delete($id){
		DB::table('t_album')->where('id_album',$id)->delete();
		DB::table('t_galery')->where('id_album',$id)->delete();
		return redirect('/admin/album')->with(['success' => 'Data Berhasil DiHapus']);;
	}

	public function detail($id){
		$data = DB::table('t_album')
		->join('t_galery','t_album.id_album','=','t_galery.id_album')
		->select('t_album.nama_album','t_album.waktu','t_galery.nama_gambar')->where('t_galery.id_album','=',$id)->get();
		return view('view_admin/galery/detail',['data' => $data]);
	}

	public function user_view(){
		$data = DB::table('t_album')
		->join('t_galery','t_album.id_album','=','t_galery.id_album')
		->select('t_album.nama_album','t_album.waktu','t_galery.nama_gambar','t_galery.id_album')->get();

		$album = DB::table('t_album')->get();
		$arr = array(
			'album'	=> $album,
			'data'	=> $data
		);
		return view('album',$arr);
	}

	public function img($id){
		$data = DB::table('t_album')
		->join('t_galery','t_album.id_album','=','t_galery.id_album')
		->select('t_album.nama_album','t_album.waktu','t_galery.nama_gambar','t_galery.id_album')->where('t_galery.id_album','=',$id)->get();
		return view('galery',['data' => $data]);
	}

	public function update(Request $req){
		$id = $req->input('id');
		$nama = $req->input('nama');
		$arr = array('nama_album' => $nama);
		if (DB::table('t_album')->where('id_album',$id)->update($arr)) {
			return redirect('/admin/album')->with(['success' => 'Ubah Data Berhasil']);
		}
	}
}
