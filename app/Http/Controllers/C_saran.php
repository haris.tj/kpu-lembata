<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class C_saran extends Controller
{
    //
	public function ks(){
		return view('buku-tamu');
	}
	public function kontak(){
		return view('kontak');
	}

	public function insert(Request $req){
		$arr = array(
			'nama'	=> $req->input('nama'),
			'email'	=> $req->input('email'),
			'alamat'	=> $req->input('alamat'),
			'pesan'	=> $req->input('pesan'),
			'tgl'	=> date('Y-m-d'),
		);

		if (DB::table('t_ks')->insert($arr)) {
			return redirect('/kontak')->with('success','Success Mingirimkan Kritik-Saran');
		}
	}

	public function viewall(){
		$data = DB::table('t_ks')->orderBy('id', 'desc')->paginate(8);
		return view('buku-tamu',['data' => $data]);
	}

	public function hapus($id){
		if (DB::table('t_ks')->delete($id)) {
			return redirect('admin/kritik-saran')->with('success','Hapus Data Berhasil');
		}
	}
	public function viewaadm(){
		$data = DB::table('t_ks')->orderBy('id', 'desc')->get();
		return view('view_admin.tamu.home',['data' => $data]);
	}
}
