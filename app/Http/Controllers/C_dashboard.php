<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use DB;

class C_dashboard extends Controller
{
    //
	public function index(){

		$berita = DB::table('t_berita')->count();
		$artikel = DB::table('t_artikel')->count();
		$album = DB::table('t_album')->count();
		$agenda = DB::table('t_agenda')->count();
		$artikel3 = DB::table('t_artikel')->orderBy('id', 'desc')->limit(3)->get();
		$berita3 = DB::table('t_berita')->orderBy('id', 'desc')->limit(3)->get();

		$data = array(
			'berita'	=> $berita,
			'artikel'	=> $artikel,
			'album'		=> $album,
			'agenda'	=> $agenda,
			'artikel3'	=> $artikel3,
			'berita3'	=> $berita3
		);
		return view('view_admin/dash',$data);
	}

}
