<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class C_uud extends Controller{

	public function index(){
		$data = DB::table('t_hukum')->get();
		return view('view_admin/hukum/home',['data' => $data]);
	}

	public function insert(Request $request){
		$validator = Validator::make($request->all(), [
			"file" => "required|mimetypes:application/pdf"
		]);

		if ($validator->fails()) {
			return redirect('/admin/hukum')->with(['error' => 'Upload Gagal.!']);
		}

		$file = $request->file('file');
		$destinationPath = "public/uploads/produk-hukum";
		$fileName = $file->getClientOriginalName();
		$request->file('file')->move($destinationPath, $fileName);

		$date = date('Y-m-d');
		$data = array(
			'nama_menu'	=> $request->input('nama'),
			'judul'		=> $request->input('judul'),
			'keterangan'=> $request->input('keterangan'),
			'file'		=> $fileName,
			'tgl_post'	=>  $date,
		);
		if (DB::table('t_hukum')->insert($data)) {
			return redirect('/admin/hukum')->with(['success' => 'Data Berhasil Ditambahkan']);
		}
//
	}

	public function detail($id){
		$data = DB::table('t_hukum')->where('id_menu',$id)->get();
		return view('view_admin/hukum/update',['data' => $data]);
	}

	public function update(Request $request){
		$validator = Validator::make($request->all(), [
			"file" => "mimetypes:application/pdf"
		]);

		if ($validator->fails()) {
			//return with(['error' => 'Upload Gambar Gagal.!']);
			return redirect('/admin/hukum/')->with(['error' => 'Upload Gagal.!']);
		}
		$file = $request->file('file');
		if (empty($file) || $file == "" || $file == null) {
			$id = $request->input('id');
			$date = date('Y-m-d');

			$data = array(
				'nama_menu'	=> $request->input('nama'),
				'judul'		=> $request->input('judul'),
				'keterangan'=> $request->input('keterangan'),
				'file'		=> $request->input('file2'),
				'tgl_post'	=>  $date,
			);
			DB::table('t_hukum')->where('id_menu',$id)->update($data);
			return redirect('/admin/hukum')->with(['success' => 'Data Berhasil Dirubah']);		
		}else{
			//img upload
			$destinationPath = "public/uploads/produk-hukum";
			$fileName = $file->getClientOriginalName();
			$request->file('file')->move($destinationPath, $fileName);

			$id = $request->input('id');
			$date = date('Y-m-d');

			$data = array(
				'nama_menu'	=> $request->input('nama'),
				'judul'		=> $request->input('judul'),
				'keterangan'=> $request->input('keterangan'),
				'file'		=> $fileName,
				'tgl_post'	=>  $date,
			);
			DB::table('t_hukum')->where('id_menu',$id)->update($data);
			return redirect('/admin/hukum')->with(['success' => 'Data Berhasil Dirubah']);
		}

	}

	public function destroy($id){
		DB::table('t_hukum')->where('id_menu',$id)->delete();
		return redirect('/admin/hukum')->with(['success' => 'Data Berhasil DiHapus']);;
	}

	public function uview(){
		return view('produk-hukum');
	}

	public function detailx($id){
		$data = DB::table('t_hukum')->where('nama_menu',$id)->get();
		echo json_encode($data);
	}
}
