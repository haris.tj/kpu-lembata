<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class C_komisioner extends Controller
{
    //
	public function index(){
		$data = DB::table('t_komisioner')->get();
		return view('view_admin/komisioner/home',['data' => $data]);
	}
	public function insertview(){
		return view('view_admin/komisioner/insert');
	}
	public function insert(Request $req){
		$validator = Validator::make($req->all(), [
			'gambar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
		]);

		if ($validator->fails()) {
			return redirect('/admin/komisioner/insert')->with(['error' => 'Upload Gambar Gagal.!']);
		}
		$file = $req->file('gambar');

		// $tmpt = $req->input('tempat_lahir');
		// $tgl = $req->input('tgl_lahir');
		// $ps = explode("-", $tgl);
		// $t = $ps[0];
		// $b = $ps[1];
		// $h = $ps[2];
		// $nama_bulan = date("F", mktime(0, 0, 0, $b, 10));
		// $dt = $tmpt." , ".$h." - ".$nama_bulan." - ".$t;

		if (empty($file) || $file == null || $file == "") {
			$data = array(
				'gambar'	=> $req->input('gambar'),
				'nama'		=> $req->input('nama'),
				// 'nip'		=> $req->input('nip'),
				'tempat_lahir'	=> $req->input('tempat_lahir'),
				'tgl_lahir'		=> $req->input('tgl_lahir'),
				'jabatan'	=> $req->input('jabatan'),
				'devisi'	=> $req->input('devisi'),
				'korwil'	=> $req->input('korwil'),
				'alamat'	=> $req->input('alamat'),
				'email'		=> $req->input('email'),
			);
			if (DB::table('t_komisioner')->insert($data)) {
				return redirect('/admin/komisioner')->with(['success' => 'Data Berhasil Ditambahkan']);
			}
		}else{
			$destinationPath = "public/komisioner";
			$fileName = $file->getClientOriginalName();
			$req->file('gambar')->move($destinationPath, $fileName);

			$data = array(
				'gambar'	=> $fileName,
				'nama'		=> $req->input('nama'),
				// 'nip'		=> $req->input('nip'),
				'tempat_lahir'	=> $req->input('tempat_lahir'),
				'tgl_lahir'		=> $req->input('tgl_lahir'),
				'jabatan'	=> $req->input('jabatan'),
				'devisi'	=> $req->input('devisi'),
				'korwil'	=> $req->input('korwil'),
				'alamat'	=> $req->input('alamat'),
				'email'		=> $req->input('email'),
			);
			if (DB::table('t_komisioner')->insert($data)) {
				return redirect('/admin/komisioner')->with(['success' => 'Data Berhasil Ditambahkan']);
			}
		}

	}

	public function delete($id){
		DB::table('t_komisioner')->where('id_komisioner',$id)->delete();
		return redirect('/admin/komisioner')->with(['success' => 'Data Berhasil DiHapus']);
	}

	public function detail($id){
		$data = DB::table('t_komisioner')->where('id_komisioner',$id)->get();
		return view('view_admin/komisioner/update',['data' => $data]);
	}

	public function update(Request $req){
		$id = $req->input('id');
		$validator = Validator::make($req->all(), [
			'gambar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
		]);

		if ($validator->fails()) {
			return redirect('/admin/komisioner/insert')->with(['error' => 'Upload Gambar Gagal.!']);
		}
		$file = $req->file('gambar');

		// $tmpt = $req->input('tempat_lahir');
		// $tgl = $req->input('tgl_lahir');
		// $ps = explode("-", $tgl);
		// $t = $ps[0];
		// $b = $ps[1];
		// $h = $ps[2];
		// dd($ps);
		// $nama_bulan = date("F", mktime(0, 0, 0, $b, 10));
		// $dt = $tmpt." , ".$h." - ".$nama_bulan." - ".$t;

		if (empty($file) || $file == null || $file == "") {
			$data = array(
				'nama'		=> $req->input('nama'),
				'gambar'	=> $req->input('img'),
				// 'nip'		=> $req->input('nip'),
				'tempat_lahir'	=> $req->input('tempat_lahir'),
				'tgl_lahir'		=> $req->input('tgl_lahir'),
				'jabatan'	=> $req->input('jabatan'),
				'devisi'	=> $req->input('devisi'),
				'alamat'	=> $req->input('alamat'),
				'email'		=> $req->input('email'),
				'korwil'	=> $req->input('korwil'),
			);
			if (DB::table('t_komisioner')->where('id_komisioner',$id)->update($data)) {
				return redirect('/admin/komisioner')->with(['success' => 'Data Berhasil Diubah']);
			}
		}else{
			$destinationPath = "public/komisioner";
			$fileName = $file->getClientOriginalName();
			$req->file('gambar')->move($destinationPath, $fileName);

			$data = array(
				'nama'		=> $req->input('nama'),
				'gambar'	=> $fileName,
				// 'nip'		=> $req->input('nip'),
				'tempat_lahir'	=> $req->input('tempat_lahir'),
				'tgl_lahir'		=> $req->input('tgl_lahir'),
				'jabatan'	=> $req->input('jabatan'),
				'devisi'	=> $req->input('devisi'),
				'alamat'	=> $req->input('alamat'),
				'email'		=> $req->input('email'),
				'korwil'	=> $req->input('korwil'),
			);
			if (DB::table('t_komisioner')->where('id_komisioner',$id)->update($data)) {
				return redirect('/admin/komisioner')->with(['success' => 'Data Berhasil Diubah']);
			}
		}

	}
	public function detailx($id){
		$data = DB::table('t_komisioner')->where('jabatan',$id)->get();
		echo json_encode($data);
		// return view('user',['data' => $data]);
	}
	public function uview(){
		$data = DB::table('t_komisioner')->get();
		return view('komisioner',['data' => $data]);
	}
}
