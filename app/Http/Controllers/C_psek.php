<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class C_psek extends Controller
{
    //
	public function index(){
		$data = DB::table('t_psek')->get();
		return view('view_admin/profilsek/home',['data' => $data]);
	}
	public function insertview(){
		return view('view_admin/profilsek/insert');
	}
	public function insert(Request $req){
		$validator = Validator::make($req->all(), [
			'gambar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
		]);

		if ($validator->fails()) {
			return redirect('/admin/user/insert')->with(['error' => 'Upload Gambar Gagal.!']);
		}
		$file = $req->file('gambar');

		// $tmpt = $req->input('tempat');
		// $tgl = $req->input('tgl');
		// $ps = explode("-", $tgl);
		// $t = $ps[0];
		// $b = $ps[1];
		// $h = $ps[2];
		// $nama_bulan = date("F", mktime(0, 0, 0, $b, 10));
		// $dt = $tmpt." , ".$h." - ".$nama_bulan." - ".$t;

		if (empty($file) || $file == null || $file == "") {
			$data = array(
				'nama'		=> $req->input('nama'),
				'gambar'	=> $req->input('gambar'),
				'nip'		=> $req->input('nip'),
				'tempat_lahir'	=> $req->input('tempat'),
				'ttl'		=> $req->input('tgl'),
				'jabatan'	=> $req->input('jabatan'),
				'alamat'	=> $req->input('alamat'),
				'email'		=> $req->input('email'),
				'telepon'	=> $req->input('telepon'),
			);
			if (DB::table('t_psek')->insert($data)) {
				return redirect('/admin/user/home')->with(['success' => 'Data Berhasil Ditambahkan']);
			}
		}else{
			$destinationPath = "public/uploads";
			$fileName = $file->getClientOriginalName();
			$req->file('gambar')->move($destinationPath, $fileName);

			$data = array(
				'nama'		=> $req->input('nama'),
				'gambar'	=> $fileName,
				'nip'		=> $req->input('nip'),
				'tempat_lahir'	=> $req->input('tempat'),
				'ttl'		=> $req->input('tgl'),
				'jabatan'	=> $req->input('jabatan'),
				'alamat'	=> $req->input('alamat'),
				'email'		=> $req->input('email'),
				'telepon'	=> $req->input('telepon'),
			);
			if (DB::table('t_psek')->insert($data)) {
				return redirect('/admin/user/home')->with(['success' => 'Data Berhasil Ditambahkan']);
			}
		}

	}

	public function delete($id){
		DB::table('t_psek')->where('id_psek',$id)->delete();
		return redirect('/admin/user/home')->with(['success' => 'Data Berhasil DiHapus']);
	}

	public function detail($id){
		$data = DB::table('t_psek')->where('id_psek',$id)->get();
		return view('view_admin/profilsek/update',['data' => $data]);
	}

	public function update(Request $req){
		$id = $req->input('id');
		$validator = Validator::make($req->all(), [
			'gambar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
		]);

		if ($validator->fails()) {
			return redirect('/admin/berita/insert')->with(['error' => 'Upload Gambar Gagal.!']);
		}
		$file = $req->file('gambar');

		// $tmpt = $req->input('tempat');
		// $tgl = $req->input('tgl');
		// $ps = explode("-", $tgl);
		// $t = $ps[0];
		// $b = $ps[1];
		// $h = $ps[2];
		// dd($ps);
		// $nama_bulan = date("F", mktime(0, 0, 0, $b, 10));
		// $dt = $tmpt." , ".$h." - ".$nama_bulan." - ".$t;

		if (empty($file) || $file == null || $file == "") {
			$data = array(
				'nama'		=> $req->input('nama'),
				'gambar'	=> $req->input('img'),
				'nip'		=> $req->input('nip'),
				'tempat_lahir'	=> $req->input('tempat'),
				'ttl'		=> $req->input('tgl'),
				'jabatan'	=> $req->input('jabatan'),
				'alamat'	=> $req->input('alamat'),
				'email'		=> $req->input('email'),
				'telepon'	=> $req->input('telepon'),
			);
			if (DB::table('t_psek')->where('id_psek',$id)->update($data)) {
				return redirect('/admin/user/home')->with(['success' => 'Data Berhasil Diubah']);
			}
		}else{
			$destinationPath = "public/uploads";
			$fileName = $file->getClientOriginalName();
			$req->file('gambar')->move($destinationPath, $fileName);

			$data = array(
				'nama'		=> $req->input('nama'),
				'gambar'	=> $fileName,
				'nip'		=> $req->input('nip'),
				'tempat_lahir'	=> $req->input('tempat'),
				'ttl'		=> $req->input('tgl'),
				'jabatan'	=> $req->input('jabatan'),
				'alamat'	=> $req->input('alamat'),
				'email'		=> $req->input('email'),
				'telepon'	=> $req->input('telepon'),
			);
			if (DB::table('t_psek')->where('id_psek',$id)->update($data)) {
				return redirect('/admin/user/home')->with(['success' => 'Data Berhasil Diubah']);
			}
		}

	}
	public function detailx($id){
		$data = DB::table('t_psek')->where('jabatan',$id)->get();
		echo json_encode($data);
		// return view('user',['data' => $data]);
	}
	public function uview(){
		return view('user');
	}
}
