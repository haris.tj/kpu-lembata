<?php


Route::get('/','C_berita@userview');

Route::get('/admin','C_dashboard@index');

Route::get('/admin/artikel/insert',function(){
	return view('view_admin/artikel');
});
Route::get('/struktur-organisasi',function(){
	return view('struktur-organisasi');
});
//LOGIN
Route::get('/admin/login','C_login@index');
Route::post('/admin/login/act','C_login@login');
Route::post('/admin/user/create','C_login@create');
Route::get('admin/token','C_login@gettoken');
Route::get('/admin/user/insert',function(){
	return view('insert');
});
Route::get('/admin/logout','C_login@logout');
Route::patch('/admin/respass','C_login@update');

// ARTIKEL
Route::post('/admin/artikel/create','C_artikel@create');
Route::get('/admin/artikel','C_artikel@index');
Route::get('/admin/artikel/delete/{id}','C_artikel@destroy');
Route::get('/admin/artikel/{id}','C_artikel@edit');
Route::post('/admin/artikel/update','C_artikel@update');
Route::get('/admin/artikel/detail/{id}','C_artikel@show');
//user artikel
Route::get('/artikel','C_artikel@userview');
Route::get('/artikel/detail/{id}','C_artikel@usrDetArtikel');
// BERITA
Route::get('/admin/berita/insert',function(){
	return view('view_admin/berita/Insert');
});
Route::post('/admin/berita/create','C_berita@create');
Route::get('/admin/berita','C_berita@index');
Route::get('/admin/berita/delete/{id}','C_berita@destroy');
Route::get('/admin/berita/{id}','C_berita@edit');
Route::post('/admin/berita/update','C_berita@update');
Route::get('/admin/berita/detail/{id}','C_berita@show');
//user berita
Route::get('/berita','C_berita@beritaview');
Route::get('/berita/detail/{id}','C_berita@usrDetBerita');

//agenda admin

Route::get('/admin/agenda','C_agenda@index');
Route::post('/admin/agenda/fnc/insert','C_agenda@insert');
Route::post('/admin/agenda/detil/{id}','C_agenda@detail');
Route::get('/admin/agenda/delete/{id}','C_agenda@delete');
Route::get('/admin/agenda/insert',function(){
	return view('view_admin/agenda/insert');
});

// Route::get('/agenda','C_agenda@view_agenda');
Route::get('/agenda/','C_agenda@view_agenda');
Route::get('/agenda/{b}/{t}','C_agenda@getagenda');
Route::patch('/agenda/update','C_agenda@updatedata');

Route::get('/admin/album','C_galery@index');
Route::patch('/admin/album/insert','C_galery@insert');
Route::patch('/admin/galery/insert','C_galery@insertIMG');
Route::get('/admin/album/delete/{id}','C_galery@delete');
Route::get('/admin/album/detail/{id}','C_galery@detail');
Route::patch('/admin/album/update','C_galery@update');

Route::get('/album','C_galery@user_view');
Route::get('/album/{id}','C_galery@img');

Route::get('/admin/user/home','C_psek@index');
Route::get('/admin/user/delete/{id}','C_psek@delete');
Route::post('/admin/user/insert/act','C_psek@insert');
Route::post('/admin/user/update','C_psek@update');
Route::get('/admin/user/insert','C_psek@insertview');
Route::get('/admin/user/detail/{id}','C_psek@detail');
Route::get('/profil','C_psek@uview');
Route::get('/profil/{id}','C_psek@detailx');


Route::get('/new_layout',function(){
	return view('test_layout');
});

Route::get('/admin/visi-misi','C_vm@index');
Route::get('/admin/visi-misi/delete/{id}','C_vm@delete');
Route::post('/admin/visi-misi/insert/act','C_vm@insert');
Route::post('/admin/visi-misi/update','C_vm@update');
Route::get('/admin/visi-misi/insert','C_vm@insertview');
Route::get('/admin/visi-misi/detail/{id}','C_vm@detail');
Route::get('/visi-misi','C_vm@userview');


Route::get('/admin/komisioner','C_komisioner@index');
Route::get('/admin/komisioner/delete/{id}','C_komisioner@delete');
Route::post('/admin/komisioner/insert/act','C_komisioner@insert');
Route::post('/admin/komisioner/update','C_komisioner@update');
Route::get('/admin/komisioner/insert','C_komisioner@insertview');
Route::get('/admin/komisioner/detail/{id}','C_komisioner@detail');
Route::get('/komisioner','C_komisioner@uview');
// Route::get('/profil/{id}','C_komisioner@detailx');

Route::get('/kontak',function(){
	return view('kontak');
});

Route::get('/admin/hukum','C_uud@index');
Route::get('/admin/hukum/delete/{id}','C_uud@destroy');
Route::patch('/admin/hukum/insert/act','C_uud@insert');
Route::patch('/admin/hukum/update','C_uud@update');
Route::get('/admin/hukum/insert','C_uud@insertview');
Route::get('/admin/hukum/detail/{id}','C_uud@detail');
Route::get('/produk-hukum','C_uud@uview');
Route::get('/produk-hukum/{id}','C_uud@detailx');
// Route::patch('/admin/hukum/insert/act','C_galery@insertIMG');


Route::get('/admin/info','C_info@index');
Route::get('/admin/info/delete/{id}','C_info@destroy');
Route::patch('/admin/info/insert/act','C_info@insert');
Route::patch('/admin/info/update','C_info@update');
Route::get('/admin/info/insert','C_info@insertview');
// Route::get('/admin/info/detail/{id}','C_uud@detail');
Route::get('/info','C_info@uview');


Route::get('/admin/pemilih','C_pemilih@index');
Route::get('/admin/pemilih/delete/{id}','C_pemilih@destroy');
Route::patch('/admin/pemilih/insert/act','C_pemilih@insert');
Route::patch('/admin/pemilih/update','C_pemilih@update');
Route::get('/admin/pemilih/insert','C_pemilih@insertview');
Route::get('/admin/pemilih/detail/{id}','C_pemilih@detail');
Route::get('/data-pemilih','C_pemilih@uview');
Route::get('/data-pemilih/{id}','C_pemilih@detailx');

Route::get('/admin/register','C_login@registerview');

Route::get('/kontak','C_saran@kontak');
Route::get('/kritik-saran','C_saran@viewall');
Route::post('/kritik-saran/send','C_saran@insert');
Route::get('/admin/kritik-saran/delete/{id}','C_saran@hapus');
Route::get('/admin/kritik-saran','C_saran@viewaadm');

Route::get('/admin/struktur','C_struktur@index');